import { Transform } from 'class-transformer';
import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import appUtilities from '../../utils/app.utilities';
import { Order } from './order.entity';

@Entity({ name: 'sold_product' })
export class SoldProduct {
  @PrimaryColumn()
  public id: string = appUtilities.generateKsuid();

  @Column({ name: 'quantity' })
  public quantity: number;

  @Column({ name: 'sku' })
  public sku: string;

  @Column({ name: 'title' })
  public title: string;

  @Transform((value) => appUtilities.getCompleteImageUrl(value), { toPlainOnly: true })
  @Transform((value) => appUtilities.getImageFilePathFromUrl(value), { toClassOnly: true })
  @Column({ name: 'thumbnail_image_url' })
  public thumbnailImageUrl: string;

  @Column({ name: 'short_description' })
  public shortDescription: string;
}
