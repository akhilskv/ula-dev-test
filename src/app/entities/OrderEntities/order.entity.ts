import { BeforeInsert, Column, Entity, OneToMany, OneToOne, PrimaryColumn } from 'typeorm';
import { OrderStatus } from '../../constants/enums/order.status.enum';
import appUtilities from '../../utils/app.utilities';
import { DatetimeEntity } from '../BaseEntities/date.entity';
import { Item } from './item.entity';
import { OrderAddress } from './order.address.entity';

@Entity({ name: 'order' })
export class Order extends DatetimeEntity {
  @PrimaryColumn()
  public id: string = appUtilities.generateOrderId();

  @Column({ name: 'total_amount' })
  public totalAmount: number;

  @Column({ name: 'shipping_amount' })
  public shippingAmount: number;

  @Column({ name: 'discount' })
  public discount: number;

  @Column({ name: 'status' })
  public status: string = OrderStatus.PENDING;

  @Column({ name: 'currency_code' })
  public currencyCode: string;

  @Column({ name: 'customer' })
  public customer: string;

  @Column({ name: 'cart_id' })
  public cartId: string;

  @Column({ name: 'delivery_time' })
  public deliveryTime: string;

  @Column({ name: 'comments' })
  public comments: string;

  @OneToMany((type: string) => Item, 'order')
  public items: Item[];

  @OneToOne((type: string) => OrderAddress, 'order')
  public address: OrderAddress;
}
