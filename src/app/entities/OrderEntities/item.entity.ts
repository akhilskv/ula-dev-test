import { Transform } from 'class-transformer';
import { Column, Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import appUtilities from '../../utils/app.utilities';
import { Order } from './order.entity';

@Entity({ name: 'order_item' })
export class Item {
  @PrimaryColumn()
  public id: string = appUtilities.generateKsuid();

  @Column({ name: 'order_id' })
  public orderId: string;

  @Column({ name: 'quantity' })
  public quantity: number;

  @Column({ name: 'price' })
  public price: number;

  @Column({ name: 'tax_percentage' })
  public taxPercentage: number;

  @Column({ name: 'discount' })
  public discount: number;

  @Column({ name: 'tax_amount' })
  public taxAmount: number;

  @Column({ name: 'total_price' })
  public totalPrice: number;

  @Column({ name: 'sku' })
  public sku: string;

  @Column({ name: 'title' })
  public title: string;

  @Transform((value) => appUtilities.getCompleteImageUrl(value), { toPlainOnly: true })
  @Transform((value) => appUtilities.getImageFilePathFromUrl(value), { toClassOnly: true })
  @Column({ name: 'thumbnail_image_url' })
  public thumbnailImageUrl: string;

  @Column({ name: 'short_description' })
  public shortDescription: string;

  @ManyToOne((type: string) => Order, 'items')
  @JoinColumn({ name: 'order_id' })
  public order: Order;
}
