import { Column, Entity, JoinColumn, OneToOne, PrimaryColumn } from 'typeorm';
import appUtilities from '../../utils/app.utilities';
import { Order } from './order.entity';

@Entity({ name: 'order_address' })
export class OrderAddress {
  @PrimaryColumn()
  public id: string = appUtilities.generateKsuid();

  @Column({ name: 'order_id' })
  public orderId: string;

  @Column({ name: 'first_name' })
  public firstName: string;

  @Column({ name: 'last_name' })
  public lastName: string;

  @Column({ name: 'middle_name' })
  public middleName: string;

  @Column({ name: 'store_name' })
  public storeName: string;

  @Column({
    nullable: true,
  })
  public building: string;

  @Column({
    nullable: true,
  })
  public road: string;

  @Column({
    nullable: true,
  })
  public town: string;

  @Column({
    nullable: true,
  })
  public village: string;

  @Column({
    nullable: true,
  })
  public street: string;

  @Column({
    nullable: true,
  })
  public location: string;

  @Column({
    nullable: true,
  })
  public subdistrict: string;

  @Column({
    nullable: true,
  })
  public district: string;

  @Column({
    nullable: true,
  })
  public city: string;

  @Column({
    nullable: true,
  })
  public state: string;

  @Column({
    nullable: true,
  })
  public region: string;

  @Column({
    nullable: true,
  })
  public province: string;

  @Column({
    nullable: true,
  })
  public zip: string;

  @Column({
    name: 'post_code',
    nullable: true,
  })
  public postCode: string;

  @Column({
    nullable: true,
  })
  public country: string;

  @Column({
    nullable: true,
  })
  public landmark: string;

  @Column({
    nullable: true,
  })
  public rt: string;

  @Column({
    nullable: true,
  })
  public rw: string;

  @Column({
    nullable: true,
  })
  public latitude: string;

  @Column({
    nullable: true,
  })
  public longitude: string;

  @Column({
    name: 'delivery_latitude',
    nullable: true,
  })
  public deliveryLatitude: string;

  @Column({
    name: 'delivery_longitude',
    nullable: true,
  })
  public deliveryLongitude: string;

  @Column({
    name: 'phone_number',
  })
  public phoneNumber: string;

  @Column({})
  public deleted: boolean;

  @OneToOne((type: string) => Order, 'address')
  @JoinColumn({ name: 'order_id' })
  public order: Order;
}
