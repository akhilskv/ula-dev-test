import { Column, CreateDateColumn, Entity, PrimaryColumn, Timestamp } from 'typeorm';
import appUtilities from '../../utils/app.utilities';

@Entity({ name: 'order_transaction' })
export class Transaction {
  @PrimaryColumn({ name: 'id' })
  public id: string = appUtilities.generateKsuid();

  @Column({ name: 'order_id' })
  public orderId: string;

  @Column()
  public status: string;

  @Column()
  public note: string;

  @Column({ name: 'created_by' })
  public createdBy: string;

  @CreateDateColumn({ name: 'created_at' })
  public createdAt: Date;
}
