import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'holiday' })
export class Holiday {
  @PrimaryGeneratedColumn('uuid')
    public id: string;

  @Column()
    public date: Date;

  @Column()
    public name: string;
}
