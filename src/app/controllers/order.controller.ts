import httpStatusCodes from 'http-status-codes';
import { asyncMiddleware } from '../middlewares/apiErrorHandler';
import orderService from '../services/order.service';
import orderTransactionService from '../services/order.transaction.service';
import soldProductService from '../services/sold.product.service';
import IController from '../types/IController';
import apiResponse from '../utils/api.response';
import orderUtilities from '../utils/order.utilities';

const createOrder: IController = asyncMiddleware(async (req, res) => {
  const order = await orderService.createOrder(orderUtilities.getOrderModelWithId(req));
  apiResponse.result(res, order, httpStatusCodes.CREATED);
});

const updateOrder: IController = asyncMiddleware(async (req, res) => {
  const { otp, ...orderModel } = req.body;
  orderModel.id = req.params.id;
  const actorId = req.headers.id as string;
  const order = await orderService.updateOrder(orderModel, actorId, otp);
  apiResponse.result(res, order, httpStatusCodes.OK);
});

const updateOrderWithoutOtpVerification: IController = asyncMiddleware(async (req, res) => {
  const orderModel = req.body;
  orderModel.id = req.params.id;
  const actorId = req.headers.id as string;
  const order = await orderService.updateOrderWithoutOtpVerification(orderModel, actorId);
  apiResponse.result(res, order, httpStatusCodes.OK);
});

const hotProducts: IController = asyncMiddleware(async (req, res) => {
  const soldProducts = await soldProductService.getBestSellingProducts();
  apiResponse.result(res, soldProducts, httpStatusCodes.OK);
});

const saveHolidays: IController = asyncMiddleware(async (req, res) => {
  await orderService.saveHolidays(req.body);
  apiResponse.result(res, {}, httpStatusCodes.OK);
});

const deliveryTime: IController = asyncMiddleware(async (req, res) => {
  const deliveryTimeObject = await orderService.getCurrentDeliveryTime();
  apiResponse.result(res, deliveryTimeObject, httpStatusCodes.OK);
});

const orderTransactions: IController = asyncMiddleware(async (req, res) => {
  const orderId = req.params.id;
  const transactions = await orderTransactionService.getOrderTransactions(orderId);
  apiResponse.result(res, transactions, httpStatusCodes.OK);
});

const updateUserDetailsInOrders: IController = asyncMiddleware(async (req, res) => {
  await orderService.updateUserDetailsToOrders();
  apiResponse.result(res, {}, httpStatusCodes.OK);
});

export default {
  createOrder,
  updateOrder,
  hotProducts,
  updateOrderWithoutOtpVerification,
  saveHolidays,
  deliveryTime,
  orderTransactions,
  updateUserDetailsInOrders,
};
