import { Request, Response } from 'express';
import httpStatusCodes from 'http-status-codes';
import { getConnection } from 'typeorm';
import logger from '../config/logger';
import { MysqlConnectionError } from '../errors/mysql.connection.error';
import { asyncMiddleware } from '../middlewares/apiErrorHandler';
import IController from '../types/IController';
import apiResponse from '../utils/api.response';

const healthCheck: IController = asyncMiddleware(async (req: Request, res: Response) => {
  await mysqlHealth();
  apiResponse.result(res, {}, httpStatusCodes.OK);
});

const mysqlHealth = async () => {
  await getConnection().query('SELECT 1')
  .catch((error) => {
    logger.error('connection to mysql failed', error);
    throw new MysqlConnectionError();
  });
  return true;
};

export default {
  healthCheck,
};
