import { createConnection } from 'typeorm';
import { appConfig } from './config/app.config';
import express from './config/express';
import logger from './config/logger';
import mysqlConfig from './config/mysqlConfig';

const PORT = appConfig.port || 5000;

createConnection(mysqlConfig as any)
.then(() => {
  logger.info('Database connection created.');
  express.listen(PORT, () => {
    logger.info(`Server running at ${PORT}`);
  });
})
.catch((error) => {
  logger.error(error);
  logger.info(`Database connection failed with error: ${error}`);
});

export default express;
