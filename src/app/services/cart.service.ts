import cartServiceConstants from '../constants/cart.service.constants';
import lambdaService from './lambda.service';

const clearCart = async (cartId: string, userId: string) => {
  await lambdaService.invokeLambdaFunction(cartServiceConstants.clearCartLambdaFunction, {
    cartId,
    userId,
  });
};

export default {
  clearCart,
};
