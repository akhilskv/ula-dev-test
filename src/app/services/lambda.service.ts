import awsSdk from 'aws-sdk';
import { awsConfig } from '../config/aws.config';
import logger from '../config/logger';

awsSdk.config.update({
  accessKeyId: awsConfig.accessKeyId,
  secretAccessKey: awsConfig.secretAccessKey,
});

const invokeLambdaFunction = async (functionName: string, dataObject?: any) => {
  const lambda = new awsSdk.Lambda();
  const params = {
    FunctionName: functionName,
    Payload: dataObject ? JSON.stringify(dataObject) : null,
  };
  return new Promise((resolve, reject) => {
    lambda.invoke(params, (err, data) => {
      if (err) {
        logger.error('Lambda invoke error', err);
        reject(err);
      } else {
        logger.info('cart service update success through lambda');
        resolve();
      }
    });
  });
};

export default {
  invokeLambdaFunction,
};
