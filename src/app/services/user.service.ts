import axios, { AxiosResponse } from 'axios';
import logger from '../config/logger';
import userServiceConfig from '../config/user.service.config';
import { BadRequestError } from '../errors/bad.request.error';
import { OtpValidationError } from '../errors/otp.validation.error';
import appUtilities from '../utils/app.utilities';

const sendOtpForCustomer = async (customer: string): Promise<AxiosResponse> => {
  try {
    const url = `${userServiceConfig.userServiceSendOtpByIdEndpoint}`;
    const config = {
      headers: {
        'Content-type': 'application/json',
        'Authorization': `Bearer ${appUtilities.generateJwtToken()}`,
      },
    };
    const data = { userId: customer };
    const response = await axios.post(url, data, config);
    logger.info(`send otp success for customer ${customer} success`);
    return response;
  } catch (error) {
    if (error.response.status === 400) {
      throw new BadRequestError(`User with id ${customer} do not have a phone number`);
    }
    throw error;
  }
};

const getUserByIds = async (userIds: string[]) => {
  try {
    const url = `${userServiceConfig.userServiceGetUsersByIdEndpoint}?ids=${JSON.stringify(userIds)}`;
    const config = {
      headers: {
        'Content-type': 'application/json',
        'Authorization': `Bearer ${appUtilities.generateJwtToken()}`,
      },
    };
    const response = await axios.get(url, config);
    logger.info(`get users by ids success  - ${userIds}`);
    return response;
  } catch (error) {
    throw error;
  }
};

const verifyOtpForCustomer = async (customer: string, otp: string): Promise<AxiosResponse> => {
  try {
    const url = `${userServiceConfig.userServiceValidateOtpEndpoint}`;
    const data = {
      otp: `${otp}`,
      userId: `${customer}`,
    };
    const config = {
      headers: {
        'Content-type': 'application/json',
        'Authorization': `Bearer ${appUtilities.generateJwtToken()}`,
      },
    };
    const response = await axios.post(url, data, config);
    logger.info('verify otp success');
    return response;
  } catch (error) {
    if (error.response.status === 403) {
      throw new OtpValidationError();
    }
    logger.error('otp verification api error');
    throw error;
  }
};

export default {
  sendOtpForCustomer,
  verifyOtpForCustomer,
  getUserByIds,
};
