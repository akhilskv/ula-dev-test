import { getCustomRepository, getRepository } from 'typeorm';
import logger from '../config/logger';
import { OrderAddress } from '../entities/OrderEntities/order.address.entity';
import { Holiday } from '../entities/OtherEntities/holiday.entity';
import { HolidayModel } from '../models/holiday.model';
import orderGraphqlModel from '../models/order.graphql.model';
import { OrderModel } from '../models/order.model';
import { UserModel } from '../models/user.model';
import { OrderRepository } from '../repositories/order.repository';
import appUtilities from '../utils/app.utilities';
import mappingUtilities from '../utils/mapping.utilities';
import soldProductService from './sold.product.service';
import userService from './user.service';

  /**
   * Create order service
   *
   * @param orderModel : OrderModel
   * @returns Order
   */
const createOrder = async (orderModel: OrderModel) => {
  await mappingUtilities.addDeliveryTime(orderModel);
  const order = await getCustomRepository(OrderRepository).createOrder(orderModel);
  soldProductService.addQuantityToSoldProducts(orderModel);
  return order;
};

  /**
   * Get current delivery time
   * @returns string
   */
const getCurrentDeliveryTime = async () => {
  let deliveryTime = await appUtilities.calculateDeliveryTime();
  deliveryTime = appUtilities.convertDbDateStringToIsoString(deliveryTime);
  return {
    deliveryTime,
  };
};

  /**
   * Service to get orders
   *
   * @param orderModel : OrderGraphqlModel
   * @returns Order[]
   */
const getOrders = async (orderModel: orderGraphqlModel) => {
  const orders = await getCustomRepository(OrderRepository).getOrders(
    orderModel.id,
    orderModel.customer,
    orderModel.status,
    orderModel.orderBy,
    orderModel.offset,
    orderModel.limit,
    orderModel.searchID,
  );
  return mappingUtilities.mapOrderEntitiesToModel(orders);
};

/**
 * Service to update order
 *
 * @param orderModel : OrderModel
 * @returns Order
 */
const updateOrder = async (orderModel: OrderModel, actorId: string, otp?: string) => {
  const order = await getCustomRepository(OrderRepository).updateOrder(orderModel, actorId, otp);
  return order;
};

/**
 * Service to update order without otp verfication
 *
 * @param orderModel : OrderModel
 * @returns Order
 */
const updateOrderWithoutOtpVerification = async (
  orderModel: OrderModel,
  actorId: string,
) => {
  const order = await getCustomRepository(OrderRepository).updateOrderWithoutOtpVerification(
    orderModel,
    actorId,
  );
  return order;
};

/**
 * Service to save the holidays
 *
 * @param holidayList : HolidayModel[]
 */
const saveHolidays = async (holidayList: HolidayModel[]) => {
  const holidayRepository = getRepository(Holiday);
  await holidayRepository.insert(holidayList);
  return;
};

/**
 * Service to get the holidays
 *
 * @returns holidayList : HolidayModel[]
 */
const getHolidaysList = async () => {
  const holidays = await getRepository(Holiday).find();
  const holidayDates = holidays.map((holiday) => {
    return holiday.date.toDateString();
  });
  return holidayDates;
};

/**
 * Service to update the user details in order which details are missing
 *
 */
const updateUserDetailsToOrders = async () => {
  const orders = await getCustomRepository(OrderRepository).getOrdersWithoutUserDetails();
  const userIds = new Set<string>();
  orders.forEach((order: { customer: string, id: string }) => {
    userIds.add(order.customer);
  });
  const response = await userService.getUserByIds(Array.from(userIds));
  const users: UserModel[] = response.data;
  const orderAddressList = mappingUtilities.mapUserDetailsToOrderAddress(orders, users);
  logger.info(`updating order addressess ${JSON.stringify(orderAddressList)}`);
  const updateResponses = await Promise.all(
    orderAddressList.map(async (orderAddress: OrderAddress) => {
      return await getRepository(OrderAddress).update(
        { orderId: orderAddress.orderId },
        orderAddress,
      );
    }),
  );
  logger.info(`Update response - ${JSON.stringify(updateResponses)}`);
};

export default {
  createOrder,
  getOrders,
  updateOrder,
  updateOrderWithoutOtpVerification,
  saveHolidays,
  getHolidaysList,
  getCurrentDeliveryTime,
  updateUserDetailsToOrders,
};
