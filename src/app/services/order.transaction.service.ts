import { getCustomRepository } from 'typeorm';
import { OrderTransactionRepository } from '../repositories/order.transaction.repository';

const getOrderTransactions = async (orderId: string) => {
  return await getCustomRepository(OrderTransactionRepository).getOrderTransactions(orderId);
};

export default {
  getOrderTransactions,
};
