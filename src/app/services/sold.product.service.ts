import axios from 'axios';
import { getCustomRepository } from 'typeorm';
import catalogServiceConfig from '../config/catalog.service.config';
import logger from '../config/logger';
import { SoldProduct } from '../entities/OrderEntities/sold.product.entity';
import { OrderModel } from '../models/order.model';
import { SoldProductRepository } from '../repositories/sold.product.repository';
import appUtilities from '../utils/app.utilities';
import graphqlUtilities from '../utils/graphql.utilities';

/**
 * Add sold product quantities
 *
 * @param orderModel : OrderModel
 */
const addQuantityToSoldProducts = async (orderModel: OrderModel): Promise<void> => {
  await getCustomRepository(SoldProductRepository).addSoldProductQuantity(orderModel);
};

/**
 * Get best selling products
 * @returns SoldProduct[]
 */
const getBestSellingProducts = async (): Promise<SoldProduct[]> => {
  const skuObjects = await getCustomRepository(
    SoldProductRepository,
  ).getBestSellingProducts();
  const skus = skuObjects.map((skuObject: { sku: string }) => skuObject.sku);
  if (!appUtilities.isArrayNotNullOrEmpty(skus)) {
    return [];
  }
  const productsObject = await getProducts(skus);
  const soldProducts = productsObject.cartProducts;
  return soldProducts;
};

const getProducts = async (skuList: string[]) => {
  const response = await axios.post(
    catalogServiceConfig.catalogServiceGetProductsBySku,
    graphqlUtilities.productListBySkuQuery(skuList),
    {
      headers: {
        'Content-type': 'application/json',
        'Authorization': `Bearer ${appUtilities.generateJwtToken()}`,
      },
    },
  ).catch((error) => {
    logger.error('product api error', error, error.response);
    throw error;
  }) as any;
  logger.info(`fetched data from product service ${JSON.stringify(response.data)}`);
  return response.data.data;
};

export default {
  addQuantityToSoldProducts,
  getBestSellingProducts,
};
