import * as express from 'express';
import graphqlRoute from './graphql.route';
import orderRoute from './order.route';

const router = express.Router();
router.use('/v1/orders', orderRoute);
router.use('/v1/graphql', graphqlRoute);
export default router;
