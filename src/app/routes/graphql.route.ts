import express from 'express';
import expressGraphql from 'express-graphql';
import { graphqlSchema } from '../graphql';

const router = express.Router();

router.use('/', expressGraphql((req: any) => ({
  schema: graphqlSchema,
  graphiql: true,
  context: req.context,
  customFormatErrorFn: (error: any) => ({
    message: error.message,
    state: error.originalError && error.originalError.state,
    statusCode: error.originalError && error.originalError.statusCode,
    errorDetails: error.originalError && error.originalError.errorDetails,
    service: error.originalError && error.originalError.service,
    locations:
      process.env.NODE_ENV === 'dev' ? error.locations : null,
    path: process.env.NODE_ENV === 'dev' ? error.path : null,
    stack: process.env.NODE_ENV === 'dev' ? error.stack : null,
  }),
}),
));

export default router;
