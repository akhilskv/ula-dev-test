import express from 'express';
import { createValidator } from 'express-joi-validation';
import { createOrderSchema } from '../constants/schema/order/order.create.schema';
import orderGetSchema from '../constants/schema/order/order.get.schema';
import { orderUpdateBodySchema, orderUpdateParamSchema, orderUpdateWithoutOtpBodySchema } from '../constants/schema/order/order.update.schema';
import orderController from '../controllers/order.controller';
import authorize from '../middlewares/authorize';

const router = express.Router();
const validator = createValidator({ passError: true });

router.post(
  '/',
  authorize(['user', 'admin', 'sales']),
  validator.body(createOrderSchema),
  orderController.createOrder,
);

router.put(
  '/userDetails',
  authorize(['admin']),
  orderController.updateUserDetailsInOrders,
);

router.put(
  '/:id',
  authorize(['admin']),
  validator.body(orderUpdateBodySchema),
  validator.params(orderUpdateParamSchema),
  orderController.updateOrder,
);

router.put(
  '/:id/statusChange',
  authorize(['admin']),
  validator.body(orderUpdateWithoutOtpBodySchema),
  validator.params(orderUpdateParamSchema),
  orderController.updateOrderWithoutOtpVerification,
);

router.get(
  '/:id/trackOrder',
  authorize(['admin', 'user']),
  validator.params(orderGetSchema.orderGetParamSchema),
  orderController.orderTransactions,
);

router.get(
  '/hotProducts',
  authorize(['user', 'sales', 'admin']),
  orderController.hotProducts,
);

router.get(
  '/deliveryTime',
  authorize(['user', 'admin']),
  orderController.deliveryTime,
);

router.post(
  '/holidays',
  authorize(['admin']),
  orderController.saveHolidays,
);

export default router;
