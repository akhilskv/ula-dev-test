import { GraphQLObjectType, GraphQLSchema } from 'graphql';
import { graphqlQueries } from './queries';

const rootQuery = new GraphQLObjectType({
  name: 'OrderQuery',
  description: 'Queries for orders backend API',
  fields: graphqlQueries,
});

export const graphqlSchema =  new GraphQLSchema({
  query: rootQuery,
});
