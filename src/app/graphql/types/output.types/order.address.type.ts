import { GraphQLObjectType, GraphQLString } from 'graphql';
const orderAddressType = new GraphQLObjectType({
  name: 'OrderAddressType',
  description: 'Graphql type for order address.',
  fields: () => ({
    id: { type: GraphQLString },
    orderId: { type: GraphQLString },
    building: { type: GraphQLString },
    street: { type: GraphQLString },
    location: { type: GraphQLString },
    city: { type: GraphQLString },
    state: { type: GraphQLString },
    zip: { type: GraphQLString },
    country: { type: GraphQLString },
    road: { type: GraphQLString },
    town: { type: GraphQLString },
    village: { type: GraphQLString },
    subdistrict: { type: GraphQLString },
    district: { type: GraphQLString },
    region: { type: GraphQLString },
    province: { type: GraphQLString },
    postCode: { type: GraphQLString },
    landmark: { type: GraphQLString },
    storeName: { type: GraphQLString },
    rw: { type: GraphQLString },
    rt: { type: GraphQLString },
    latitude: { type: GraphQLString },
    longitude: { type: GraphQLString },
  }),
});

export default orderAddressType;
