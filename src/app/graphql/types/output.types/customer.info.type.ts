import { GraphQLObjectType, GraphQLString } from 'graphql';
const customerInfoType = new GraphQLObjectType({
  name: 'OrderCustomerInfoType',
  description: 'Graphql type for order address.',
  fields: () => ({
    firstName: { type: GraphQLString },
    lastName: { type: GraphQLString },
    middleName: { type: GraphQLString },
    phoneNumber: { type: GraphQLString },
  }),
});

export default customerInfoType;
