import {
    GraphQLFloat,
    GraphQLInt,
    GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLString } from 'graphql';
import { GraphQLDateTime } from 'graphql-iso-date';
import orderAddressType from '../output.types/order.address.type';
import customerInfoType from './customer.info.type';
import itemType from './item.type';

const  orderType = new GraphQLObjectType({
  name: 'Order',
  description: 'GraphQL schema for Order',
  fields: () => ({
    id: { type: GraphQLString },
    customer: { type: GraphQLString },
    status: { type: GraphQLString },
    currencyCode: { type: GraphQLString },
    comments: { type: GraphQLString },
    shippingAmount: { type: GraphQLFloat },
    totalAmount: { type: GraphQLFloat },
    items: { type: GraphQLList(itemType) },
    address: { type: orderAddressType },
    customerInfo: { type: customerInfoType },
    deliveryTime: { type: GraphQLDateTime },
    createdAt: { type: GraphQLDateTime },
    updatedAt: { type: GraphQLDateTime },
  }),
});

export default orderType;
