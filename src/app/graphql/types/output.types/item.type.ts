import {
    GraphQLFloat,
    GraphQLInt,
    GraphQLObjectType,
    GraphQLString } from 'graphql';

const itemType = new GraphQLObjectType({
  name: 'Item',
  description: 'GraphQL schema for Item',
  fields: () => ({
    id: { type: GraphQLString },
    title: { type: GraphQLString },
    sku: { type: GraphQLString },
    name: { type: GraphQLString },
    quantity: { type: GraphQLInt },
    price: { type: GraphQLFloat },
    totalPrice: { type: GraphQLFloat },
    thumbnailImageUrl: { type: GraphQLString },
    shortDescription: { type: GraphQLString },
  }),
});

export default itemType;
