import { Request } from 'express';
import { GraphQLInt, GraphQLList, GraphQLString } from 'graphql';
import graphqlAuthorize from '../../middlewares/graphql.authorize';
import { asyncGraphqlMiddleware } from '../../middlewares/graphql.error.handler';
import orderGraphqlModel from '../../models/order.graphql.model';
import orderService from '../../services/order.service';
import orderType from '../types/output.types/order.type';

const adminOrderQuery = {
  adminOrders: {
    type: GraphQLList(orderType),
    description: 'Query to fetch orders from database.',
    args: {
      id: {
        type: GraphQLString,
      },
      status: {
        type: GraphQLString,
      },
      customer: {
        type: GraphQLString,
      },
      orderBy: {
        type: GraphQLString,
      },
      offset: {
        type: GraphQLInt,
      },
      limit: {
        type: GraphQLInt,
      },
      searchID: {
        type: GraphQLString,
      },
    },
    resolve: asyncGraphqlMiddleware(
        async (
            parent: any,
            fields: orderGraphqlModel,
            req: Request,
        ) => {
          graphqlAuthorize(req, ['admin']);
          const orders =  await orderService.getOrders(fields);
          return orders;
        }),

  },
};

export default adminOrderQuery;
