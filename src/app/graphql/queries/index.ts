import adminOrderQuery from './admin.order.query';
import orderQuery from './order.query';

export const graphqlQueries = {
  ...orderQuery,
  ...adminOrderQuery,
};
