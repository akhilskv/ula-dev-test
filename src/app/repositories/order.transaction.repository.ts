import { getRepository, EntityRepository, Repository } from 'typeorm';
import { Transaction } from '../entities/OrderEntities/transaction.entity';

@EntityRepository(Transaction)
export class OrderTransactionRepository extends Repository<Transaction> {
  /**
   * Get order transaction by order id
   *
   * @returns Transaction[]
   */
  public async getOrderTransactions(orderId: string): Promise<Transaction[]> {
    return await getRepository(Transaction).find({ orderId });
  }
}
