import { classToPlain, plainToClass } from 'class-transformer';
import lodash from 'lodash';
import { getRepository, EntityRepository, Repository } from 'typeorm';
import logger from '../config/logger';
import application from '../constants/application';
import { SoldProduct } from '../entities/OrderEntities/sold.product.entity';
import { ItemModel } from '../models/item.model';
import { OrderModel } from '../models/order.model';

@EntityRepository(SoldProduct)
export class SoldProductRepository extends Repository<SoldProduct> {
  /**
   * Add quantity to the soldProducts table
   *
   * @param orderModel : OrderModel
   */
  public async addSoldProductQuantity(orderModel: OrderModel): Promise<void> {
    try {
      const items = orderModel.items;
      if (items) {
        const skuList = items.map((item: ItemModel) => item.sku);
        const query = getRepository(SoldProduct)
          .createQueryBuilder(SoldProduct.name)
          .andWhere(`${SoldProduct.name}.sku IN (:skuList)`, { skuList });
        const soldProducts = await query.getMany();
        const soldProductEntities = this.createSoldProductEntities(items, soldProducts);
        getRepository(SoldProduct).save(soldProductEntities);
      }
    } catch (error) {
      logger.error(`Sold product update failed with error - ${error}`);
    }
  }

  /**
   * Get best selling products
   *
   * @returns SoldProduct[]
   */
  public async getBestSellingProducts(): Promise<SoldProduct[]> {
    const bestSellingProducts = await getRepository(SoldProduct)
      .createQueryBuilder(SoldProduct.name)
      .select([`${SoldProduct.name}.sku`])
      .orderBy(`${SoldProduct.name}.quantity`, 'DESC')
      .limit(application.bestSellingProductLimit)
      .getMany();
    return bestSellingProducts;
  }

  /*
    Create or update sold product entities
  */
  private createSoldProductEntities(items: ItemModel[], soldProducts: SoldProduct[]) {
    const soldProductEntities: SoldProduct[] = [];
    items.forEach((item: ItemModel) => {
      const soldProductEntity = plainToClass(SoldProduct, item);
      const existingItem = lodash.find(soldProducts, (soldProduct: SoldProduct) => {
        return soldProduct.sku === item.sku;
      });
      if (existingItem) {
        soldProductEntity.id = existingItem.id;
        soldProductEntity.quantity = existingItem.quantity + soldProductEntity.quantity;
      }
      soldProductEntities.push(soldProductEntity);
    });
    return soldProductEntities;
  }
}
