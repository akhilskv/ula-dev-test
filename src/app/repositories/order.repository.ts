import { classToPlain, plainToClass } from 'class-transformer';
import lodash from 'lodash';
import {
  getCustomRepository,
  getManager,
  getRepository,
  Brackets,
  EntityManager,
  EntityRepository,
  Repository,
  SelectQueryBuilder,
} from 'typeorm';
import constants from '../constants';
import orderConstants from '../constants/OrderConstants/order.constants';
import { OrderStatus } from '../constants/enums/order.status.enum';
import { Item } from '../entities/OrderEntities/item.entity';
import { OrderAddress } from '../entities/OrderEntities/order.address.entity';
import { Order } from '../entities/OrderEntities/order.entity';
import { Transaction } from '../entities/OrderEntities/transaction.entity';
import { BadRequestError } from '../errors/bad.request.error';
import { EmptyItemsError } from '../errors/empty.items.error';
import { EntityNotFoundError } from '../errors/entity.not.found';
import { OrderNotFoundError } from '../errors/order.not.found.error';
import { ItemModel } from '../models/item.model';
import { OrderAddressModel } from '../models/order.address.model';
import orderGraphqlModel from '../models/order.graphql.model';
import { CustomerInfo, OrderModel, UpdateOrderModel } from '../models/order.model';
import { TransactionModel } from '../models/transaction.model';
import cartService from '../services/cart.service';
import userService from '../services/user.service';
import appUtilities from '../utils/app.utilities';
import orderUtilities from '../utils/order.utilities';

@EntityRepository(Order)
export class OrderRepository extends Repository<Order> {
  /**
   * Create order
   *
   * @param orderModel : OrderModel
   * @returns Order
   */
  public async createOrder(orderModel: OrderModel): Promise<Order> {
    try {
      const orderWithTotalAmount = orderUtilities.calculateOrderTotalAmount(
        orderModel,
      );
      const {
        items: items,
        address: orderAddress,
        customerInfo: customerInfo,
        ...order
      } = orderWithTotalAmount;
      let orderEntity = plainToClass(Order, order);
      let itemsEntity: Item[];
      await getManager().transaction(async (transactionalEntityManager) => {
        await transactionalEntityManager.insert(Order, orderEntity);
        if (appUtilities.isArrayNotNullOrEmpty(items)) {
          itemsEntity = await this.insertItems(items, transactionalEntityManager, orderEntity);
        } else {
          throw new EmptyItemsError();
        }
        await this.insertTransaction(
          orderEntity,
          transactionalEntityManager,
          orderEntity.customer,
        );
        await this.insertAddress(
          orderAddress,
          customerInfo,
          transactionalEntityManager,
          orderEntity,
        );
        await cartService.clearCart(orderModel.cartId, orderModel.customer);
      });
      orderEntity = {
        ...orderEntity,
        items: itemsEntity,
        address: orderAddress as any,
      };
      return orderEntity;
    } catch (err) {
      throw err;
    }
  }

  /**
   * Get all orders based on the filter
   *
   * @param orderId : string
   * @param customerId : string
   * @param status : string
   * @param orderBy: string
   * @param offset : offset for the list
   * @param limit : limit for the list
   * @returns Order[]
   */
  public async getOrders(
    orderId: string,
    customerId: string,
    status: OrderStatus,
    orderBy: string,
    offset: number = 0,
    limit: number = 10,
    searchID: string,
  ): Promise<Order[]> {
    try {
      const query = getRepository(Order)
        .createQueryBuilder(Order.name)
        .select(`${Order.name}.id`);
      this.addIdFilterToQuery(query, orderId);
      this.addCustomerFilterToQuery(query, customerId);
      this.addStatusFilterToQuery(query, status);
      this.addOrderByToQuery(query, orderBy);
      this.addSearchIdToQuery(query, searchID);

      query.skip(offset).take(limit);
      const orderIdEntities = await query.getMany();

      const orderIds = orderIdEntities.map((order: { id: string }) => order.id);
      let entitiesWithItems: Order[];
      if (appUtilities.isArrayNotNullOrEmpty(orderIds)) {
        entitiesWithItems = await this.getOrderItemsAndAddress(
          orderIds,
          orderBy,
        );
      } else {
        entitiesWithItems = [];
      }

      return classToPlain(entitiesWithItems) as any;
    } catch (error) {
      throw error;
    }
  }

  /**
   * Update order
   *
   * @param orderModel : UpdateOrderModel
   * @param actorId: string
   * @param otp: string
   * @returns order: Order
   */
  public async updateOrder(
    orderModel: UpdateOrderModel,
    actorId: string,
    otp?: string,
  ): Promise<Order> {
    try {
      let order: Order;
      await getManager().transaction(async (transactionalEntityManager) => {
        order = await transactionalEntityManager.findOneOrFail(Order, {
          id: orderModel.id,
        });
        if (!orderUtilities.validStatus(orderModel.status, order.status)) {
          throw new BadRequestError(constants.Messages.INVALID_UPDATE_STATUS);
        }
        if (orderUtilities.statusIsDelivered(orderModel)) {
          await userService.sendOtpForCustomer(order.customer);
        } else if (orderUtilities.statusIsReceived(orderModel)) {
          if (otp) {
            await userService.verifyOtpForCustomer(order.customer, otp);
          } else {
            throw new BadRequestError(constants.Messages.OTP_NOT_PRESENT);
          }
        }
        await this.performUpdation(
          orderModel,
          order.status,
          actorId,
          transactionalEntityManager,
        );
      });
      order.status = orderModel.status;
      return order;
    } catch (err) {
      if (err.name === constants.ErrorCodes.ENTITY_NOT_FOUND) {
        throw new OrderNotFoundError();
      }
      throw err;
    }
  }

  /**
   * Update order
   *
   * @param orderModel : UpdateOrderModel
   * @param actorId: string
   * @param otp: string
   * @returns order: Order
   */
  public async updateOrderWithoutOtpVerification(
    orderModel: UpdateOrderModel,
    actorId: string,
  ): Promise<Order> {
    try {
      let order: Order;
      await getManager().transaction(async (transactionalEntityManager) => {
        order = await transactionalEntityManager.findOneOrFail(Order, {
          id: orderModel.id,
        });
        if (!orderUtilities.validStatus(orderModel.status, order.status)) {
          throw new BadRequestError(constants.Messages.INVALID_UPDATE_STATUS);
        }
        await this.performUpdation(
          orderModel,
          order.status,
          actorId,
          transactionalEntityManager,
        );
      });
      order.status = orderModel.status;
      return order;
    } catch (err) {
      if (err.name === constants.ErrorCodes.ENTITY_NOT_FOUND) {
        throw new OrderNotFoundError();
      }
      throw err;
    }
  }

  /*
  Get order id and customer id of the orders whose address
  details d not have firstname
  */
  public async getOrdersWithoutUserDetails() {
    const query = getRepository(Order)
      .createQueryBuilder(Order.name)
      .select([`${Order.name}.id`, `${Order.name}.customer`]);
    const subQuery = query
      .subQuery()
      .select('orderAddress.order_id')
      .distinct(true)
      .from(OrderAddress, 'orderAddress')
      .where(`orderAddress.deleted = false`)
      .andWhere(`orderAddress.firstName is NULL`)
      .getQuery();
    return await query.andWhere(`${Order.name}.id IN ${subQuery}`).getMany();
  }

  private async getOrderItemsAndAddress(orderIds: string[], orderBy: string) {
    const query = getRepository(Order)
      .createQueryBuilder(Order.name)
      .whereInIds(orderIds)
      .leftJoinAndSelect(`${Order.name}.items`, Item.name)
      .leftJoinAndSelect(
        `${Order.name}.address`,
        OrderAddress.name,
        `${OrderAddress.name}.deleted = false`,
      );
    this.addOrderByToQuery(query, orderBy);
    const orderEntities = await query.getMany();
    return orderEntities;
  }

  /**
   * Add order by query
   */
  private addOrderByToQuery(query: SelectQueryBuilder<Order>, orderBy: string) {
    if (orderBy) {
      return query.orderBy(
        `${Order.name}.createdAt`,
        orderBy === orderConstants.ORDERBY_ASCENDING ? 'ASC' : 'DESC',
      );
    }
    return query.orderBy(`${Order.name}.createdAt`, 'DESC');
  }

  /**
   * Add searchID to query
   */

  private addSearchIdToQuery(
    query: SelectQueryBuilder<Order>,
    searchID: string,
  ) {
    if (searchID) {
      const subQuery = this.getUserDetailsSearchQuery(query, searchID);
      query.andWhere(
        new Brackets((qb) => {
          qb.where(`${Order.name}.customer =  :searchID`, {
            searchID,
          })
            .orWhere(`${Order.name}.id =  :searchID`, { searchID })
            .orWhere(`${Order.name}.id IN ${subQuery}`);
        }),
      );
    }
    return query;
  }

  /**
   * Add order search by user firstName, lastName or storeName
   */
  private getUserDetailsSearchQuery(
    query: SelectQueryBuilder<Order>,
    searchID: string,
  ) {
    if (searchID) {
      const searchTerms = appUtilities.splitTermIntoFullTextEntities(searchID);
      const subQuery = query
        .subQuery()
        .select('orderAddress.order_id')
        .distinct(true)
        .from(OrderAddress, 'orderAddress')
        .where(`orderAddress.deleted = false`);
      return subQuery
        .andWhere(
          new Brackets((qb) => {
            qb.where(
              `MATCH(orderAddress.firstName) AGAINST ('${searchTerms}' IN BOOLEAN MODE)`,
            )
              .orWhere(
                `MATCH(orderAddress.lastName) AGAINST ('${searchTerms}' IN BOOLEAN MODE)`,
              )
              .orWhere(
                `MATCH(orderAddress.storeName) AGAINST ('${searchTerms}' IN BOOLEAN MODE)`,
              );
          }),
        )
        .getQuery();
    }
    return null;
  }

  /**
   * Add id filter query
   */
  private addIdFilterToQuery(query: SelectQueryBuilder<Order>, id: string) {
    if (id) {
      query.andWhere(`   ${Order.name}.id =   :id`, { id });
    }
    return query;
  }

  /**
   * Add customer id filter query
   */
  private addCustomerFilterToQuery(
    query: SelectQueryBuilder<Order>,
    customer: string,
  ) {
    if (customer) {
      query.andWhere(`   ${Order.name}.customer =   :customer`, { customer });
    }
    return query;
  }

  /**
   * Add status filter query
   */
  private addStatusFilterToQuery(
    query: SelectQueryBuilder<Order>,
    status: OrderStatus,
  ) {
    if (status && status === orderConstants.ORDER_PSEUDO_STATUS_OPEN) {
      const openStatus = lodash.difference(Object.values(OrderStatus), [
        OrderStatus.RECEIVED,
        OrderStatus.REJECTED,
        OrderStatus.FAILED_DELIVERY,
        OrderStatus.CANCELLED,
      ]);
      query.andWhere(`${Order.name}.status IN  (:openStatus)`, { openStatus });
    } else if (status && status !== orderConstants.ORDER_PSEUDO_STATUS_OPEN) {
      query.andWhere(`${Order.name}.status =   :status`, { status });
    }
    return query;
  }

  /**
   * Insert items to the db
   */
  private async insertItems(
    items: ItemModel[],
    transactionalEntityManager: EntityManager,
    order: Order,
  ): Promise<Item[]> {
    if (items.length === 0) {
      throw new BadRequestError(
        `Order with Id: ${order.id} does not contain any items.`,
      );
    }
    const itemsEntity: Item[] = [];
    items.forEach((item: ItemModel) => {
      const itemEntity = plainToClass(Item, item);
      itemEntity.orderId = order.id;
      itemsEntity.push(itemEntity);
    });
    await transactionalEntityManager.save(itemsEntity);
    return itemsEntity;
  }

  private async performUpdation(
    orderUpdations: UpdateOrderModel,
    previousStatus: string,
    actorId: string,
    transactionManager: EntityManager,
  ) {
    await transactionManager.update(
      Order,
      { id: orderUpdations.id },
      orderUpdations,
    );
    if (
      orderUtilities.hasSkippedDeliveredStatus(
        previousStatus,
        orderUpdations.status,
      )
    ) {
      await this.insertTransaction(
        { ...orderUpdations, status: OrderStatus.DELIVERED },
        transactionManager,
        actorId,
      );
    }
    await this.insertTransaction(orderUpdations, transactionManager, actorId);
  }

  /**
   * Insert address to the db
   */
  private async insertAddress(
    orderAddressModel: OrderAddressModel,
    customerInfo: CustomerInfo,
    transactionalEntityManager: EntityManager,
    order: Order,
  ): Promise<OrderAddress> {
    const orderAddressModelWithCustomer = {
      ...orderAddressModel,
      ...customerInfo,
    };
    const orderAddress = plainToClass(
      OrderAddress,
      orderAddressModelWithCustomer,
    );
    orderAddress.orderId = order.id;
    await transactionalEntityManager.save(orderAddress);
    return;
  }

  /**
   * Insert order transaction to the db
   */
  private async insertTransaction(
    order: Order | UpdateOrderModel,
    transactionalEntityManager: EntityManager,
    createdBy: string,
  ): Promise<Transaction> {
    const transaction: Transaction = new Transaction();
    transaction.orderId = order.id;
    transaction.status = order.status;
    transaction.createdBy = createdBy;
    await transactionalEntityManager.save(transaction);
    return;
  }
}
