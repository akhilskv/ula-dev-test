import { Request } from 'express';
// tslint:disable-next-line: no-duplicate-imports
import express from 'express';
import jsonwebtoken from 'jsonwebtoken';
import logger from '../config/logger';
import { UnauthorizedError } from '../errors/unauthorized.error';

const getTokenString = (req: express.Request) => {
  const tokenWithBearerHeader = req.header('authorization');
  if (tokenWithBearerHeader) {
    return tokenWithBearerHeader.replace('Bearer ', '');
  }
  return '';
};

const setUserIdAsHeader = (req: Request, id: string) => {
  req.headers.id = id;
};

const setUserRoleAsHeader = (req: Request, role: string) => {
  req.headers.role = role;
};

const authorizeRoles = (req: Request, roles: string[]) => {
  const token = getTokenString(req);
  const data: any = jsonwebtoken.decode(token);
  if (data) {
    const role = data['custom:role'] as string;
    if (roles.includes(role)) {
      logger.log('info', 'authorized');
      setUserIdAsHeader(req, data['custom:id']);
      setUserRoleAsHeader(req, role);
    } else {
      logger.error(`unauthorized - ${role} is not authorized for this api`);
      throw new UnauthorizedError();
    }
  } else {
    logger.error('unauthorized - invalid data in token');
    throw new UnauthorizedError();
  }
};

export default {
  setUserIdAsHeader,
  setUserRoleAsHeader,
  getTokenString,
  authorizeRoles,
};
