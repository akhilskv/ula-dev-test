import jsonwebtoken from 'jsonwebtoken';
import ksuid from 'ksuid';
import momentTimezone from 'moment-timezone';
import { appConfig } from '../config/app.config';
import userServiceConfig from '../config/user.service.config';
import orderConstants from '../constants/OrderConstants/order.constants';
import application from '../constants/application';
import roles from '../constants/roles';
import orderUtilities from './order.utilities';

const generateKsuid = () => {
  return ksuid.randomSync().string;
};

const isArrayNotNullOrEmpty = (inputArray: any[]) => {
  return inputArray && inputArray.length > 0;
};

const getCompleteImageUrl = (filepath: string) => {
  if (filepath) {
    return `${appConfig.imagesStoreHost}/${filepath}`;
  }
  return filepath;
};

const getImageFilePathFromUrl = (url: string) => {
  if (url) {
    return url.replace(`${appConfig.imagesStoreHost}/`, '');
  }
  return url;
};

const generateOrderId = () => {
  const timePart = (new Date()).getTime().toString();
  const randomPart = Math.floor(Math.random() * 100);
  return `${orderConstants.ORDER_ID_PRESTRING}${timePart}${randomPart}`;
};

const calculateDeliveryTime = async () => {
  const dateStringDefaultTimezone = momentTimezone
    .utc()
    .tz(application.defaultTimeZone)
    .format(application.dateFormat);
  let nextDay  = new Date(dateStringDefaultTimezone);
  nextDay = await orderUtilities.getValidDeliveryDate(nextDay);
  const nextDayString = momentTimezone
    .utc(nextDay.toISOString())
    .tz(application.defaultTimeZone, true)
    .format();
  const utcString = momentTimezone.utc(nextDayString).format(application.dbDateTimeFormat);
  return utcString;
};

/**
 * split the serach word into
 * full text splits
 * coca cola into cola* coca*
 *
 */
const splitTermIntoFullTextEntities = (searchTerm: string) => {
  const searchWordList = searchTerm.split(' ');
  return searchWordList.reduce((fullTextSearchTerm: string, word: string) => {
    if (word) {
      return `${word}* ${fullTextSearchTerm}`;
    }
    return fullTextSearchTerm;
  },                           '');
};

const convertDbDateStringToIsoString = (datestring: string) => {
  const newDateString = momentTimezone.utc(datestring).format();
  return newDateString;
};

const generateJwtToken = () => {
  const token = jsonwebtoken.sign(
    { 'custom:role': roles.SERVICE },
    userServiceConfig.jwtTokenSecret,
    {
      expiresIn: 60,
    },
  );
  return token;
};

const skipDeliveryDateIfHoliday = (deliveryDate: Date,
                                   holidayList: string[]) => {
  if (holidayList.includes(deliveryDate.toDateString()) || deliveryDate.getDay() === 0) {

    // Skip one more day
    deliveryDate.setDate(deliveryDate.getDate() + 1);
    skipDeliveryDateIfHoliday(deliveryDate, holidayList);
  }
  return;
};

export default {
  generateKsuid,
  generateOrderId,
  generateJwtToken,
  calculateDeliveryTime,
  isArrayNotNullOrEmpty,
  getCompleteImageUrl,
  getImageFilePathFromUrl,
  skipDeliveryDateIfHoliday,
  convertDbDateStringToIsoString,
  splitTermIntoFullTextEntities,
};
