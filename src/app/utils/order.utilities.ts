import express from 'express';
import { validPreStatus, OrderStatus } from '../constants/enums/order.status.enum';
import { Item } from '../entities/OrderEntities/item.entity';
import { ItemModel } from '../models/item.model';
import { OrderModel } from '../models/order.model';
import orderService from '../services/order.service';
import appUtilities from './app.utilities';

const calculateOrderTotalAmount = (order: OrderModel): OrderModel => {
  const items = order.items;
  order.totalAmount = 0;
  order.discount = 0;
  items.forEach((item: Item) => {
    item.totalPrice = calculateTotalAmount(item);
    order.totalAmount = order.totalAmount + item.totalPrice;
  });
  order.items = items;
  return order;
};

const calculateTotalAmount = (item: ItemModel) => {
  const totalPrice = item.price * item.quantity;
  return totalPrice;
};

const getOrderModelWithId = (req: express.Request): OrderModel => {
  const newOrder: OrderModel = req.body;
  newOrder.customer = req.header('id') as string;
  return newOrder;
};

const validStatus = (newStatus: string, previousStatus: string) => {
  if (validPreStatus[newStatus] && validPreStatus[newStatus].includes(previousStatus as any)) {
    return true;
  }
  return false;
};

const statusIsDelivered = (order: { status: string }): boolean => {
  return order.status === OrderStatus.DELIVERED;
};

const statusIsReceived = (order: { status: string }): boolean => {
  return order.status === OrderStatus.RECEIVED;
};

const hasSkippedDeliveredStatus = (previousStatus: string, currentStatus: string) => {
  if (previousStatus === OrderStatus.SHIPPED && currentStatus === OrderStatus.RECEIVED) {
    return true;
  }
  return false;
};

const getValidDeliveryDate = async (orderedDate: Date) => {
  const holidayList = await orderService.getHolidaysList();

  let orderedOnHoliday = false;
  if (holidayList.includes(orderedDate.toDateString()) || orderedDate.getDay() === 0) {
    orderedOnHoliday = true;
  }

  // skip to next day as per the default condition and skip if that day is holiday
  orderedDate.setDate(orderedDate.getDate() + 1);
  appUtilities.skipDeliveryDateIfHoliday(orderedDate,
                                         holidayList);
  if (orderedOnHoliday) {
    // Skip to the next day and skip if that day is also a holiday
    orderedDate.setDate(orderedDate.getDate() + 1);
    appUtilities.skipDeliveryDateIfHoliday(orderedDate,
                                           holidayList);
  }
  return orderedDate;
};

export default {
  calculateOrderTotalAmount,
  getOrderModelWithId,
  validStatus,
  statusIsDelivered,
  statusIsReceived,
  hasSkippedDeliveredStatus,
  getValidDeliveryDate,
};
