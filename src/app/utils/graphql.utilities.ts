const productListBySkuQuery = (skuList: string[]) => {
  const skus = skuList.map((item: string) => `"${item}"`);
  return {
    query: `query{
  cartProducts( skuList: [${skus}]){
    sku
    price
    currencyCode
    status
    thumbnailImageUrl
    title
    shortDescription
  }
}`,
  };
};

export default {
  productListBySkuQuery,
};
