import { Response } from 'express';
import httpStatusCodes from 'http-status-codes';

export default class ApiResponse {
  public static result = (res: Response, data: object, status: number = 200) => {
    res.status(status);
    res.json(data);
  }

  public static error = (
    res: Response,
    status: number = 400,
    error: string = httpStatusCodes.getStatusText(status),
    name: string,
  ) => {
    res.statusMessage = error;
    res.status(status).json({
      name,
      message: error,
    });
  }
}
