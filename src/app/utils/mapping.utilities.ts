import { classToPlain, plainToClass } from 'class-transformer';
import { OrderAddress } from '../entities/OrderEntities/order.address.entity';
import { Order } from '../entities/OrderEntities/order.entity';
import { OrderModel } from '../models/order.model';
import { UserModel } from '../models/user.model';
import appUtilities from '../utils/app.utilities';

const addDeliveryTime = async (orderModel: OrderModel) => {
  const deliveryTime =  await appUtilities.calculateDeliveryTime();
  orderModel.deliveryTime = deliveryTime;
};

/**
 * Map user details into order address
 *
 */
const mapUserDetailsToOrderAddress = (orders: Order[], users: UserModel[]) => {
  return orders.reduce(
    (
      orderAddressList: OrderAddress[],
      order: { id: string; customer: string },
    ) => {
      const user: UserModel = users.find((userModel: UserModel) => {
        return userModel.id === order.customer;
      });
      if (user) {
        const orderAddress = plainToClass(OrderAddress, {
          firstName: user.firstName,
          lastName: user.lastName,
          storeName: user.address ? user.address.storeName : null,
          phoneNumber: user.phoneNumber,
          orderId: order.id,
        });
        delete orderAddress.id;
        orderAddressList.push(orderAddress);
      }
      return orderAddressList;
    },
    [],
  );
};

/**
 * Map order entities to model
 *
 */
const mapOrderEntitiesToModel = (orders: Order[]) => {
  return orders.map((order: Order) => {
    const orderModel = classToPlain(order) as OrderModel;
    const { customerInfo, orderAddressModel } = mapAddressToUserDetails(order.address);
    orderModel.address = orderAddressModel;
    orderModel.customerInfo = customerInfo;
    return orderModel;
  });
};

/**
 * Map order address to customer info
 *
 */
const mapAddressToUserDetails = (orderAddress: OrderAddress) => {
  if (orderAddress) {
    const orderAddressObject = classToPlain(orderAddress) as any;
    const {
      firstName,
      lastName,
      middleName,
      phoneNumber,
      ...orderAddressModel
    } = orderAddressObject;
    const customerInfo = {
      firstName,
      lastName,
      middleName,
      phoneNumber,
    };
    return {
      customerInfo,
      orderAddressModel,
    };
  }
  return null;
};

export default {
  addDeliveryTime,
  mapUserDetailsToOrderAddress,
  mapOrderEntitiesToModel,
};
