// tslint:disable-next-line: no-var-requires
const dotenv = require('./dotenv');

export const appConfig = {
  port: process.env.PORT,
  imagesStoreHost: process.env.IMAGE_STORE_HOST,
};
