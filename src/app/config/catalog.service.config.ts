// tslint:disable-next-line: no-var-requires
const dotenv = require('./dotenv');

export default {
  catalogServiceGetProductsBySku: process.env.PRODUCT_SERVICE_GRAPHQL_API_ENDPOINT,
  jwtTokenSecret: process.env.JWT_TOKEN_SECRET,
};
