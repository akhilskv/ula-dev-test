// tslint:disable-next-line: no-var-requires
const _ = require('./dotenv');

const mysqlConfig = {
  type: 'mysql',
  replication: {
    master: {
      host: process.env.DB_HOST,
      username: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
    },
    slaves: [{
      host: process.env.DB_HOST_READ,
      username: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
    }],
  },
  charset: 'utf8',
  driver: 'mysql',
  timezone: '+0',
  synchronize: false,
  entities: ['dist/**/*.entity.js'],
  logging: process.env.NODE_ENV !== 'production' ? 'all' : 'error',
  migrations: ['dist/app/migration/*.js'],
  cli: {
    migrationsDir: 'src/app/migration',
  },
  connectTimeout: 60 * 60 * 1000,
  acquireTimeout: 60 * 60 * 1000,
  timeout: 60 * 60 * 1000,
  dropSchema: process.env.DB_DROP_SCHEMA === 'true' ? true : false,
  migrationsRun: process.env.DB_MIGRATIONS_RUN === 'true' ? true : false,
};

export =  mysqlConfig;
