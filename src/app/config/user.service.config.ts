// tslint:disable-next-line: no-var-requires
const dotenv = require('./dotenv');

export default {
  userServiceSendOtpByIdEndpoint: process.env.USER_SERVICE_SEND_OTP_ENDPOINT,
  userServiceValidateOtpEndpoint: process.env.USER_SERVICE_VALIDATE_OTP_ENDPOINT,
  userServiceGetUsersByIdEndpoint: process.env.USER_SERVICE_GET_USERS_BY_ID_ENDPOINT,
  jwtTokenSecret: process.env.JWT_TOKEN_SECRET,
};
