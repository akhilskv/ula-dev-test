// tslint:disable-next-line: no-var-requires
const dotenv = require('./dotenv');

export const awsConfig = {
  accessKeyId: process.env.AWS_ACCESS_KEY,
  secretAccessKey: process.env.AWS_SECRET_KEY,
  region: process.env.AWS_REGION,
  clearCartLambdaFunction: process.env.CLEAR_CART_LAMBDA_FUNCTION,
};
