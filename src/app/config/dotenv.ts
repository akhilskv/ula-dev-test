import dotenvSafe from 'dotenv-safe';
import path from 'path';

dotenvSafe.load({
  allowEmptyValues: true,
  path: path.join(__dirname, `../../../.env.${process.env.NODE_ENV}`),
});

export default dotenvSafe;
