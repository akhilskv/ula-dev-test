import * as bodyParser from 'body-parser';
import express from 'express';
// tslint:disable-next-line: no-var-requires
const dotenv = require('./dotenv');
import application from '../constants/application';

import cors from 'cors';
import morgan from 'morgan';
import * as errorHandler from '../middlewares/apiErrorHandler';
import joiErrorHandler from '../middlewares/joiErrorHandler';
import healthRoute from '../routes/health.route';
import indexRoute from '../routes/index.route';

const app = express();

app.use(bodyParser.json());

app.use(morgan('dev'));
app.use(cors());
// Router
app.use('/', healthRoute);
app.use(application.url.base, indexRoute);
// Joi Error Handler
app.use(joiErrorHandler);
// Error Handler
app.use(errorHandler.notFoundErrorHandler);

app.use(errorHandler.errorHandler);

export default app;
