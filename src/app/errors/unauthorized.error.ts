import httpStatusCodes from 'http-status-codes';
import logger from '../config/logger';
import { BaseError } from './base.error';

export class UnauthorizedError extends BaseError {
  public static staticMessage = 'Unauthorized';
  public statusCode: number;
  constructor(error?: string) {
    const message = error ?  error : UnauthorizedError.staticMessage;
    super(message);
    this.name = 'UnauthorizedError';
    this.statusCode = httpStatusCodes.UNAUTHORIZED;
    this.message = UnauthorizedError.staticMessage;
    logger.error(this.message);
  }
}
