import httpStatusCodes from 'http-status-codes';
import logger from '../config/logger';
import { BaseError } from './base.error';

export class EmptyItemsError extends BaseError {
  public static message = 'Items are empty';
  public statusCode: number;
  constructor(message?: string) {
    super(message);
    this.message = message ? message : EmptyItemsError.message;
    this.statusCode = httpStatusCodes.BAD_REQUEST;
    this.message = message;
    logger.error(this.message);
  }
}
