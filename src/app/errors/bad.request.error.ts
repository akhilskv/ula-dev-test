import httpStatusCodes from 'http-status-codes';
import logger from '../config/logger';
import { BaseError } from '../errors/base.error';

export class BadRequestError extends BaseError {
  public static staticMessage = 'Bad request';
  public statusCode: number;
  constructor(error?: string) {
    const message = error ? error : BadRequestError.staticMessage;
    super(message);
    this.statusCode = httpStatusCodes.BAD_REQUEST;
    this.message = message;
    logger.error(this.message);
  }
}
