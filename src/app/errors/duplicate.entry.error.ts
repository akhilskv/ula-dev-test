import httpStatusCodes from 'http-status-codes';
import logger from '../config/logger';
import { BaseError } from './base.error';

export class DuplicateEntryError extends BaseError {
  public static staticMessage = 'Entry already exists.';
  public statusCode: number;
  constructor(error?: string) {
    const message = error ? error : DuplicateEntryError.staticMessage;
    super(message);
    this.statusCode = httpStatusCodes.FORBIDDEN;
    this.message = message;
    logger.error(this.message);
  }
}
