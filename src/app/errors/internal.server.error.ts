import httpStatusCodes from 'http-status-codes';
import logger from '../config/logger';
import { BaseError } from './base.error';

export class InternalServerError extends BaseError {
  public static staticMessage = 'Internal server error';
  public statusCode: number;
  constructor(error?: string) {
    const message = error ?  error : InternalServerError.staticMessage;
    super(message);
    this.name = 'InternalServerError';
    this.statusCode = httpStatusCodes.INTERNAL_SERVER_ERROR;
    this.message = message;
    logger.error(this.message);
  }
}
