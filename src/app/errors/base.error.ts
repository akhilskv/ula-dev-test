import logger from '../config/logger';

export class BaseError extends Error {
  public statusCode: number;
  constructor(error: any) {
    super(error);
  }
}
