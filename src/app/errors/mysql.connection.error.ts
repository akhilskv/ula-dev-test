import httpStatusCodes from 'http-status-codes';
import logger from '../config/logger';
import { BaseError } from './base.error';

export class MysqlConnectionError extends BaseError {
  public static staticMessage = 'Mysql connection error';
  constructor(error?: string) {
    const message = error ?  error : MysqlConnectionError.staticMessage;
    super(message);
    this.name = 'MysqlConnectionError';
    this.message = message;
    this.statusCode = httpStatusCodes.INTERNAL_SERVER_ERROR;
    logger.error(this.message);
  }
}
