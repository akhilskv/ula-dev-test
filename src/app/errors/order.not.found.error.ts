import httpStatusCodes from 'http-status-codes';
import logger from '../config/logger';
import { BaseError } from './base.error';

export class OrderNotFoundError extends BaseError {
  public static staticMessage = 'Order does not exists.';
  public statusCode: number;
  constructor(error?: string) {
    super(error);
    this.message = error ? error : OrderNotFoundError.staticMessage;
    this.statusCode = httpStatusCodes.NOT_FOUND;
    logger.error(this.message);
  }
}
