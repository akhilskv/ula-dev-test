import { FORBIDDEN } from 'http-status-codes';
import logger from '../config/logger';
import { BaseError } from './base.error';

export class OtpValidationError extends BaseError {
  public static staticMessage =
  'The code you entered is wrong. Please check the code and try again';
  constructor(error?: string) {
    const message = error ?  error : OtpValidationError.staticMessage;
    super(message);
    this.name = 'OtpValidationError';
    this.message = message;
    this.statusCode = FORBIDDEN;
    logger.error(this.message);
  }
}
