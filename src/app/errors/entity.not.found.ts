import httpStatusCodes from 'http-status-codes';
import logger from '../config/logger';
import { BaseError } from './base.error';

export class EntityNotFoundError extends BaseError {
  public static staticMessage = 'Entity does not exists.';
  public statusCode: number;
  constructor(error?: string) {
    super(error);
    this.message = error ? error : EntityNotFoundError.staticMessage;
    this.statusCode = httpStatusCodes.NOT_FOUND;
    logger.error(this.message);
  }
}
