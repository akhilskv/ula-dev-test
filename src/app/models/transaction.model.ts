export  interface TransactionModel {
  id: string;
  orderId: string;
  status: string;
  note: string;
  createdBy: string;
  createdAt: Date;
}
