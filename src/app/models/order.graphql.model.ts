import { OrderStatus } from '../constants/enums/order.status.enum';

export default interface OrderGraphQLModel {
  id: string;
  status: OrderStatus;
  statuses: string;
  orderBy: string;
  customer: string;
  offset: number;
  limit: number;
  searchID: string;
}
