export interface UserModel {
  firstName: string;
  lastName: string;
  middleName: string;
  role: string;
  phoneNumber: string;
  id: string;
  address: any;
  email: string;
  avatar: string;
  storeImageUrls: string[];
  createdBy: string;
  createdByUser: any;
}

export interface AddressModel {
  id: string;
  userId: string;
  building: string;
  road: string;
  town: string;
  village: string;
  street: string;
  location: string;
  subdistrict: string;
  district: string;
  city: string;
  state: string;
  region: string;
  province: string;
  zip: string;
  postCode: string;
  country: string;
  landmark: string;
  rt: number;
  rw: number;
  latitude: string;
  longitude: string;
}
