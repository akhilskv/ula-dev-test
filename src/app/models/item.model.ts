export interface ItemModel {
  orderId: string;
  quantity: number;
  price: number;
  taxPercentage: number;
  taxAmount: number;
  discount: number;
  totalPrice: number;
  sku: string;
  title: string;
  shortDescription: string;
  thumbnailImageUrl: string;
}
