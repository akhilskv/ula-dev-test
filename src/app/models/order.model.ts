import { ItemModel } from './item.model';
import { OrderAddressModel } from './order.address.model';

export interface OrderModel {
  id: string;
  totalAmount: number;
  shippingAmount: number;
  discount: number;
  status: string;
  currencyCode: string;
  customer: string;
  items: ItemModel[];
  address: OrderAddressModel;
  customerInfo: CustomerInfo;
  cartId: string;
  comments: string;
  deliveryTime: string;
}

export interface CustomerInfo {
  firstName: string;
  lastName: string;
  middleName: string;
  phoneNumber: string;
}

export interface UpdateOrderModel {
  status: string;
  id: string;
  otp?: string;
}
