export interface OrderAddressModel {
  id: string;
  orderId: string;
  building: string;
  storeName: string;
  road: string;
  town: string;
  village: string;
  street: string;
  location: string;
  subdistrict: string;
  district: string;
  city: string;
  state: string;
  region: string;
  province: string;
  zip: string;
  postCode: string;
  country: string;
  landmark: string;
  rt: number;
  rw: number;
  latitude: string;
  longitude: string;
}
