export interface HolidayModel {
  id: string;
  date: Date;
  name?: string;
}
