import { Request } from 'express';
import middlewareUtilities from '../utils/middleware.utilities';

const authorize = (req: Request, roles: string[]) => {
  middlewareUtilities.authorizeRoles(req, roles);
};

export default authorize;
