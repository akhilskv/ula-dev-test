import express from 'express';
import * as HttpStatus from 'http-status-codes';
import logger from '../config/logger';
import errors from '../constants/errors';
import { IError } from './apiErrorHandler';

interface IJoiErrorDetail {
  message?: string;
  path?: string;
}

interface IJoiError extends IError {
  isJoi?: boolean;
  details?: IJoiErrorDetail[];
}
/**
 * Joi error handler middleware
 *
 * @param {object} err
 * @param {object} req
 * @param {object} res
 * @param {function} next
 *
 */
export default (
  err: any,
  req: express.Request,
  res: express.Response,
  next: express.NextFunction,
) => {
  if (err.error && err.error.isJoi) {
    const joiError = err.error;
    const error = {
      code: HttpStatus.BAD_REQUEST,
      message: HttpStatus.getStatusText(HttpStatus.BAD_REQUEST),
      details: joiError.details,
      name: errors.BAD_REQUEST,
    };
    logger.error(error);
    return res.status(HttpStatus.BAD_REQUEST).json(error);
  }
  // If this isn't a Joi error, send it to the next error handler
  return next(err);
};
