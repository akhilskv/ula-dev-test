import express from 'express';
import jsonwebtoken from 'jsonwebtoken';
import logger from '../config/logger';
import { UnauthorizedError } from '../errors/unauthorized.error';
import middlewareUtilities from '../utils/middleware.utilities';

const authorize = (roles: string[]) => {
  return (req: express.Request, res: express.Response, next: express.NextFunction) => {
    const token = middlewareUtilities.getTokenString(req);
    const data: any = jsonwebtoken.decode(token);
    if (data) {
      const role = data['custom:role'] as string;
      if (roles.includes(role)) {
        logger.log('info', 'authorized');
        middlewareUtilities.setUserIdAsHeader(req, data['custom:id']);
        next();
      } else {
        logger.error(`unauthorized - ${role} is not authorized for this api`);
        next(new UnauthorizedError());
      }
    } else {
      logger.error('unauthorized - invalid data in token');
      next(new UnauthorizedError());
    }
  };
};

export default authorize;
