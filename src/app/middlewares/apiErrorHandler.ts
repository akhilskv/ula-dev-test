import express from 'express';
import httpStatusCodes from 'http-status-codes';
import logger from '../config/logger';
import constants from '../constants';
import { BaseError } from '../errors/base.error';
import apiResponse from '../utils/api.response';

export interface IError {
  status?: number;
  code?: number;
  message?: string;
}
/**
 * NOT_FOUND(404) middleware to catch error response
 *
 * @param  {object}   req
 * @param  {object}   res
 * @param  {function} next
 */
export function notFoundErrorHandler(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction,
) {
  res.status(httpStatusCodes.NOT_FOUND).json({
    success: false,
    error: {
      code: httpStatusCodes.NOT_FOUND,
      message: httpStatusCodes.getStatusText(httpStatusCodes.NOT_FOUND),
    },
  });
}

/**
 * Generic error response middleware
 *
 * @param  {object}   err
 * @param  {object}   req
 * @param  {object}   res
 * @param  {function} next
 */
export function errorHandler(
  err: BaseError,
  req: express.Request,
  res: express.Response,
  next: express.NextFunction,
) {
  logger.error(err);
  res.status(err.statusCode || httpStatusCodes.INTERNAL_SERVER_ERROR).json({
    message: err.message || httpStatusCodes.getStatusText(httpStatusCodes.INTERNAL_SERVER_ERROR),
    name: err.name,
  });
}

/**
 * Generic error catch middleware
 *
 * @param  {function} fn
 * @param  {object}   req
 * @param  {object}   res
 */
export const asyncMiddleware = (fn: (arg0: any, arg1: any) => any) => async (
  req: express.Request,
  res: express.Response,
) => {
  await fn(req, res).catch((error: BaseError | any) => {
    logger.error(error.stack);
    logger.error(error);
    if (error instanceof BaseError) {
      apiResponse.error(res, error.statusCode, error.message, error.name);
    } else {
      apiResponse.error(
        res,
        httpStatusCodes.INTERNAL_SERVER_ERROR,
        'Internal server error',
        constants.ErrorCodes.INTERNAL_SERVER_ERROR,
      );
    }
  });
};
