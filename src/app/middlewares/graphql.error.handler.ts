import { Request } from 'express';
import logger from '../config/logger';
import { BaseError } from '../errors/base.error';
import { InternalServerError } from '../errors/internal.server.error';
import { UnauthorizedError } from '../errors/unauthorized.error';

export const asyncGraphqlMiddleware = (fn: (arg0: any, arg1: any, arg2: any) => any) => async (
  parent: any, fields: any, req: Request,
) => {
  return await fn(parent, fields, req).catch((error: BaseError | any) => {
    logger.error(error.stack);
    logger.error(error);
    if (error instanceof BaseError) {
      throw new UnauthorizedError(error.message);
    } else {
      throw new InternalServerError();
    }
  });
};
