import { awsConfig } from '../config/aws.config';

export default {
  clearCartLambdaFunction: awsConfig.clearCartLambdaFunction,
};
