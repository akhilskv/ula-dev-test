export default {
  INVALID_UPDATE_STATUS: 'Incorrect order update status.',
  OTP_NOT_PRESENT: 'OTP not present',
};
