import joi from '@hapi/joi';

const orderUpdateBodySchema = joi.object({
  status: joi.string().trim().required(),
  otp: joi.string().trim(),
});

const orderUpdateWithoutOtpBodySchema = joi.object({
  status: joi.string().trim().required(),
});

const orderUpdateParamSchema = joi.object({
  id: joi.string().trim().required(),
});

export {
  orderUpdateParamSchema,
  orderUpdateBodySchema,
  orderUpdateWithoutOtpBodySchema,
};
