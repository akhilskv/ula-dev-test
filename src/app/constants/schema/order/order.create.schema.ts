import joi from '@hapi/joi';
import currencyCodes from '../../currency.codes';
import addressSchema from '../common/address.schema';
// tslint:disable-next-line: import-name

const joiEmptyString = joi
  .string()
  .trim()
  .allow('', null);

const createOrderSchema = joi.object({
  currencyCode: joi
    .string()
    .trim()
    .valid(currencyCodes.IDR, currencyCodes.INR)
    .required(),
  cartId: joi
  .string().required(),
  shippingAmount: joi.number().default(0),
  comments: joi.string().max(200).allow(null, ''),
  items: joi
    .array()
    .items(
      joi.object({
        quantity: joi.number().required(),
        price: joi.number().required(),
        taxPercentage: joi.number(),
        taxAmount: joi.number(),
        discount: joi.number(),
        sku: joi
          .string()
          .trim()
          .required(),
        title: joi
          .string()
          .trim()
          .required(),
        shortDescription: joi.string().trim(),
        thumbnailImageUrl: joi
          .string()
          .uri()
          .trim()
          .required(),
      }),
    )
    .required(),
  address: addressSchema.required(),
  customerInfo: joi.object({
    firstName: joiEmptyString,
    lastName: joiEmptyString,
    middleName: joiEmptyString,
    phoneNumber: joiEmptyString,
  }).allow(null),
});

export { createOrderSchema };
