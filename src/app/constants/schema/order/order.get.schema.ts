import joi from '@hapi/joi';

const orderGetParamSchema = joi.object({
  id: joi.string().trim().required(),
});

export default {
  orderGetParamSchema,
};
