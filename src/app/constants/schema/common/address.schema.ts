import joi from '@hapi/joi';

const joiEmptyString = joi
  .string()
  .trim()
  .allow('', null);

export default joi.object({
  building:  joiEmptyString,
  road:  joiEmptyString,
  town:  joiEmptyString,
  village:  joiEmptyString,
  street:  joiEmptyString,
  location:  joiEmptyString,
  subdistrict:  joiEmptyString,
  district:  joiEmptyString,
  city:  joiEmptyString,
  state:  joiEmptyString,
  region:  joiEmptyString,
  province:  joiEmptyString,
  zip:  joiEmptyString,
  postCode:  joiEmptyString,
  country:  joiEmptyString,
  landmark:  joiEmptyString,
  rt: joi.string().trim().length(3).regex(/^\d+$/).allow('', null),
  rw: joi.string().trim().length(3).regex(/^\d+$/).allow('', null),
  latitude: joi.string().trim().allow('', null),
  longitude: joi.string().trim().allow('', null),
  deliveryLatitude: joi.string().trim().allow('', null),
  deliveryLongitude: joi.string().trim().allow('', null),
  storeName: joiEmptyString,
}).min(1);
