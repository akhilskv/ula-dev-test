import currencyCodes from './currency.codes';
import errors from './errors';
import messages from './messages';

export default {
  ErrorCodes: errors,
  Messages: messages,
  CurrencyCodes: currencyCodes,
};
