export enum OrderStatus {
  PENDING = 'PENDING',
  SOURCED = 'SOURCED',
  IN_TRANSIT = 'IN_TRANSIT',
  SHIPPED = 'SHIPPED',
  DELIVERED = 'DELIVERED',
  RECEIVED = 'RECEIVED',
  CANCELLED = 'CANCELLED',
  REJECTED = 'REJECTED',
  RE_SHIPPED = 'RE_SHIPPED',
  FAILED_DELIVERY = 'FAILED_DELIVERY',
}

export const validPreStatus: { [key: string]: OrderStatus[] } = {
  SHIPPED: [OrderStatus.PENDING],
  DELIVERED: [OrderStatus.SHIPPED],
  RECEIVED: [OrderStatus.DELIVERED, OrderStatus.SHIPPED],
  REJECTED: [OrderStatus.SHIPPED],
  FAILED_DELIVERY: [OrderStatus.SHIPPED],
};
