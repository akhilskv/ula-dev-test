export default {
  ENTITY_NOT_FOUND: 'EntityNotFound',
  INVALID_UPDATE_ERROR: 'InvalidUpdateError',
  INTERNAL_SERVER_ERROR: 'InternalServerError',
  DUPLICATE_ENTRY_ERROR: 'ER_DUP_ENTRY',
  BAD_REQUEST: 'BadRequestError',
};
