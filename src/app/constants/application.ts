const base = '/';
const application = {
  url: {
    base,
  },
  cache: {
    CATEGORY_EN_KEY: 'category_en',
  },
  bestSellingProductLimit: 10,
  defaultTimeZone: 'Asia/Jakarta',
  dbDateTimeFormat: 'YYYY-MM-DD HH:mm:ss',
  dateFormat: 'YYYY-MM-DD',
};

export default application;
