import { Index, MigrationInterface, QueryRunner, Table, TableIndex } from 'typeorm';
import { OrderStatus } from '../constants/enums/order.status.enum';

export class OrdersMigration1575872425984 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.createTable(
      new Table({
        name: 'order',
        columns: [
          {
            name: 'id',
            type: 'varchar',
            isPrimary: true,
            isGenerated: false,
          },
          {
            name: 'total_amount',
            type: 'decimal',
          },
          {
            name: 'shipping_amount',
            type: 'decimal',
          },
          {
            name: 'discount',
            type: 'decimal',
          },
          {
            name: 'status',
            type: 'enum',
            enum: Object.values(OrderStatus),
          },
          {
            name: 'currency_code',
            type: 'varchar',
          },
          {
            name: 'customer',
            type: 'varchar',
          },
          {
            name: 'cart_id',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'delivery_time',
            type: 'timestamp',
            isNullable: true,
          },
          {
            type: 'varchar',
            name: 'comments',
            isNullable: true,
          },
          {
            name: 'created_at',
            type: 'timestamp',
            default: 'CURRENT_TIMESTAMP',
          },
          {
            name: 'updated_at',
            type: 'timestamp',
            default: 'CURRENT_TIMESTAMP',
          },
        ],
      }),
      true,
    );

    await queryRunner.createIndex(
      'order',
      new TableIndex({
        name: 'ORDER_CUSTOMER_INDEX',
        columnNames: ['customer'],
      }),
    );

    await queryRunner.createIndex(
      'order',
      new TableIndex({
        name: 'ORDER_TIMESTAMP_INDEXES',
        columnNames: ['created_at', 'updated_at'],
      }),
    );

    await queryRunner.createTable(
      new Table({
        name: 'order_item',
        columns: [
          {
            name: 'id',
            type: 'varchar',
            isPrimary: true,
            isGenerated: false,
          },
          {
            name: 'order_id',
            type: 'varchar',
          },
          {
            name: 'quantity',
            type: 'int',
          },
          {
            name: 'price',
            type: 'decimal',
            precision: 17,
            scale: 2,
          },
          {
            name: 'tax_percentage',
            type: 'decimal',
            default: 0,
          },
          {
            name: 'tax_amount',
            type: 'decimal',
            default: 0,
          },
          {
            name: 'discount',
            type: 'decimal',
            default: 0,
          },
          {
            name: 'total_price',
            type: 'decimal',
            precision: 17,
            scale: 2,
          },
          {
            name: 'sku',
            type: 'varchar',
          },
          {
            name: 'title',
            type: 'varchar',
          },
          {
            name: 'thumbnail_image_url',
            type: 'varchar',
          },
          {
            name: 'short_description',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'created_at',
            type: 'timestamp',
            default: 'CURRENT_TIMESTAMP',
          },
        ],
      }),
      true,
    );

    await queryRunner.createIndex(
      'order_item',
      new TableIndex({
        name: 'ORDER_ITEM_PRODUCT_INDEX',
        columnNames: ['title', 'sku'],
      }),
    );

    await queryRunner.createTable(
      new Table({
        name: 'order_transaction',
        columns: [
          {
            name: 'id',
            type: 'varchar',
            isPrimary: true,
            isGenerated: false,
          },
          {
            name: 'order_id',
            type: 'varchar',
            isNullable: false,
          },
          {
            name: 'status',
            type: 'enum',
            enum: Object.values(OrderStatus),
          },
          {
            name: 'note',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'created_by',
            type: 'varchar',
          },
          {
            name: 'created_at',
            type: 'timestamp',
            default: 'CURRENT_TIMESTAMP',
          },
        ],
      }),
      true,
    );

    await queryRunner.createIndex(
      'order_transaction',
      new TableIndex({
        name: 'ORDER_TRANSACTION_INDEX',
        columnNames: ['order_id', 'created_by'],
      }),
    );

    await queryRunner.createTable(
      new Table({
        name: 'order_address',
        columns: [
          {
            name: 'id',
            type: 'varchar',
            isGenerated: false,
            isPrimary: true,
          },
          {
            name: 'order_id',
            type: 'varchar',
            isNullable: false,
            isUnique: true,
          },
          {
            name: 'building',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'road',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'town',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'village',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'street',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'location',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'subdistrict',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'district',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'city',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'state',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'region',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'post_code',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'province',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'zip',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'landmark',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'country',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'rt',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'rw',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'latitude',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'longitude',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'deleted',
            type: 'boolean',
            isNullable: false,
            default: false,
          },
        ],
      }),
      true,
    );

    await queryRunner.createIndex(
      'order_address',
      new TableIndex({
        name: 'ORDER_ADDRESS_INDEX',
        columnNames: ['order_id'],
      }),
    );

    await queryRunner.createTable(
      new Table({
        name: 'sold_product',
        columns: [
          {
            name: 'id',
            type: 'varchar',
            isPrimary: true,
            isGenerated: false,
          },
          {
            name: 'sku',
            type: 'varchar',
          },
          {
            name: 'quantity',
            type: 'int',
          },
          {
            name: 'title',
            type: 'varchar',
          },
          {
            name: 'thumbnail_image_url',
            type: 'varchar',
          },
          {
            name: 'short_description',
            type: 'varchar',
            isNullable: true,
          },
        ],
      }),
    );

    await queryRunner.createIndex(
      'sold_product',
      new TableIndex({
        name: 'SOLD_PRODUCT_INDEX',
        columnNames: ['sku'],
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropIndex('sold_product', 'SOLD_PRODUCT_INDEX');
    await queryRunner.dropTable('sold_product');
    await queryRunner.dropIndex('order_transaction', 'ORDER_TRANSACTION_INDEX');
    await queryRunner.dropTable('order_transaction');
    await queryRunner.dropIndex('order_address', 'ORDER_ADDRESS_INDEX');
    await queryRunner.dropTable('order_address');
    await queryRunner.dropIndex('order_item', 'ORDER_ITEM_PRODUCT_INDEX');
    await queryRunner.dropTable('order_item');
    await queryRunner.dropIndex('order', 'ORDER_CUSTOMER_INDEX');
    await queryRunner.dropIndex('order', 'ORDER_TIMESTAMP_INDEXES');
    await queryRunner.dropTable('order');
  }
}
