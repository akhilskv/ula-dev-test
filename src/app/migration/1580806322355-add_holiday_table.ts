import { MigrationInterface, QueryRunner, Table, TableIndex } from 'typeorm';

export class AddHolidayTable1580806322355 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.createTable(new Table({
      name: 'holiday',
      columns: [{
        name: 'id',
        type: 'varchar',
        isPrimary: true,
        isGenerated: true,
      },
      {
        name: 'date',
        type: 'date',
        isNullable: false,
      },
      {
        name: 'name',
        type: 'varchar',
        isNullable: true,
      },
      ],
    }),                           true);

    await queryRunner.createIndex(
        'holiday',
        new TableIndex({
          name: 'HOLIDAY_DATE_INDEX',
          columnNames: ['date'],
        }),
      );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropIndex('holiday', 'HOLIDAY_DATE_INDEX');
    await queryRunner.dropTable('holiday');
  }

}
