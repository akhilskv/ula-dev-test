import { MigrationInterface, QueryRunner, TableIndex } from 'typeorm';

export class UserDetailsFullText1582250074613 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.createIndex(
      'order_address',
      new TableIndex({
        name: 'USER_FIRST_NAME_FULLTEXT_INDEX',
        columnNames: ['first_name'],
        isFulltext: true,
      }),
    );
    await queryRunner.createIndex(
      'order_address',
      new TableIndex({
        name: 'USER_LAST_NAME_FULLTEXT_INDEX',
        columnNames: ['last_name'],
        isFulltext: true,
      }),
    );
    await queryRunner.createIndex(
      'order_address',
      new TableIndex({
        name: 'USER_STORE_NAME_FULLTEXT_INDEX',
        columnNames: ['store_name'],
        isFulltext: true,
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropIndex('order_address', 'USER_FIRST_NAME_FULLTEXT_INDEX');
    await queryRunner.dropIndex('order_address', 'USER_LAST_NAME_FULLTEXT_INDEX');
    await queryRunner.dropIndex('order_address', 'USER_STORE_NAME_FULLTEXT_INDEX');
  }
}
