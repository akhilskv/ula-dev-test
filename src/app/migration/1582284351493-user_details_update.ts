import { MigrationInterface, QueryRunner } from 'typeorm';
import orderService from '../services/order.service';

export class UserDetailsUpdate1582284351493 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    if (process.env.NODE_ENV !== 'test') {
      await orderService.updateUserDetailsToOrders();
    }
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    // EMPTY BLOCK
  }
}
