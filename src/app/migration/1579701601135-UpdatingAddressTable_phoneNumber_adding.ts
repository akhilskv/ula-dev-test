import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class UpdatingAddressTablePhoneNumberAdding1579701601135 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.addColumn('order_address', new TableColumn({
      name: 'phone_number',
      type: 'varchar',
      isNullable: true,
    }));
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropColumn('order_address', 'phone_number');

  }

}
