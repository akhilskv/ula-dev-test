import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddDeliveryLatLong1580377661597 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.addColumns('order_address', [
      new TableColumn({
        name: 'delivery_latitude',
        type: 'varchar',
        isNullable: true,
      }),
      new TableColumn({
        name: 'delivery_longitude',
        type: 'varchar',
        isNullable: true,
      }),
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropColumn('order_address', 'delivery_latitude');
    await queryRunner.dropColumn('order_address', 'delivery_longitude');
  }

}
