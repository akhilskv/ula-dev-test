import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddressNameUpdate1579605811689 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.addColumns('order_address', [
      new TableColumn({
        name: 'first_name',
        type: 'varchar',
        isNullable: true,
      }),
      new TableColumn({
        name: 'last_name',
        type: 'varchar',
        isNullable: true,
      }),
      new TableColumn({
        name: 'middle_name',
        type: 'varchar',
        isNullable: true,
      }),
      new TableColumn({
        name: 'store_name',
        type: 'varchar',
        isNullable: true,
      }),
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropColumn('order_address', 'store_name');
    await queryRunner.dropColumn('order_address', 'middle_name');
    await queryRunner.dropColumn('order_address', 'last_name');
    await queryRunner.dropColumn('order_address', 'first_name');
  }
}
