import jsonwebtoken from 'jsonwebtoken';
import { Order } from '../../app/entities/OrderEntities/order.entity';
import { getRepository } from 'typeorm';
import logger from '../../app/config/logger';
import express from 'express';
import { OrderStatus } from '../../app/constants/enums/order.status.enum';
import { stat } from 'fs';

const generateTokenForRole = (role: string, customer: string) => {
  const tokenPayload = {
    'custom:role': role,
    'custom:id': customer,
  };
  return jsonwebtoken.sign(tokenPayload, 'secretKey');
};

const clearOrderData = async () => {
  await getRepository(Order).clear();
};

const createMultipleOrders = async (
  createFunction: (url: string, data: any, token:string) => void,
  args: any,
  orderCount: number) => {
  const newOrders: Order[] = [];
  try {
    for (let i = 0; i < orderCount; i = i + 1) {
      const res: any = await createFunction(args.url, args.orderData, args.token);
      newOrders.push(res.body as Order);
    }
    logger.log('info', `Successfully created ${orderCount} orders`);
    return newOrders;
  } catch (error) {
    logger.error(`Failed to created ${orderCount} orders`);
  }
};

const updateAnOrderToReceivedStatus = async (
  updateFunction: (id: string, data: any, token?: string) => void,
  args: any) => {

  try {
    const statuses = ['SHIPPED', 'DELIVERED', 'RECEIVED'];
    for (let i = 0; i < statuses.length; i = i + 1) {
      args.updateOrderData.status = statuses[i];
      const res: any = await updateFunction(args.id, args.updateOrderData, args.token);
      logger.info(res);
    }
    logger.log('info', `Successfully updated Order: ${args.id} to "recieved"`);
  } catch (error) {
    logger.error(`Failed to update Order: ${args.id}`);
  }
};

export default {
  generateTokenForRole,
  clearOrderData,
  createMultipleOrders,
  updateAnOrderToReceivedStatus,
};
