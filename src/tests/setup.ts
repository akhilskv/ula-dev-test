import { createConnection, createConnections } from 'typeorm';
import logger from '../app/config/logger';
import mysqlConfig from '../app/config/mysqlConfig';

before((done: any) => {
  createConnection(mysqlConfig as any)
    .then(() => {
      logger.log('info', 'test db connected');
      done();
    })
    .catch((error) => {
      logger.error(error);
      logger.info(`Database connection failed with error ${error}`);
      done();
    });
});
