import typeormMock from '../dummies/typeorm.mock';
const proxyquire = require('proxyquire').noCallThru();

describe('Get orders', () => {
  it('Should get orders and map the data to orderModel', async () => {
    const offset = 2;
    const limit = 4;

    const orderServiceWithTypeormMock = proxyquire(
      '../../app/services/order.service',
      { typeorm: typeormMock },
    ).default;

    const result = await orderServiceWithTypeormMock.getOrders({
      offset,
      limit,
    } as any);
    result.should.have.lengthOf(1);
    result[0].id.should.equals('OD123456789896776');
    result[0].customerInfo.phoneNumber.should.equals('+9100099000000');
    result[0].customerInfo.firstName.should.equals('test user');
    result[0].customerInfo.lastName.should.equals('test user last name');
    result[0].address.should.not.have.property('firstName');
  });
});
