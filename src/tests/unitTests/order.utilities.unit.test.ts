import appUtilities from '../../app/utils/app.utilities';
import orderUtilities from '../../app/utils/order.utilities';
import chai from 'chai';
import sinon from 'sinon';

import orderService from '../../app/services/order.service';
const should = chai.should();
const expect = chai.expect;

describe('Utils Unit tests', () => {
  describe('Valid delivery dates testing', () => {

    const holidayListString = [
      new Date(2020, 1, 4),
      new Date(2020, 1, 7),
      new Date(2020, 1, 8),
      new Date(2020, 1, 11),
      new Date(2020, 1, 13),
      new Date(2020, 1, 14),
  ].map((holiday) => {
    return holiday.toDateString();
  });
    sinon.stub(orderService, 'getHolidaysList').returns(Promise.resolve(holidayListString));

    it('Delivery date is next working day if order placed on working day and next day is holiday',
       async () => {
         const orderDate = new Date(2020, 1, 3);
         const newDeliveryDate = await orderUtilities.getValidDeliveryDate(orderDate);
         newDeliveryDate.toISOString().should.equals(new Date(2020, 1, 5).toISOString());
       });

    it('Delivery date is next working date + 1 if order placed on holiday', async () => {
      const orderDate = new Date(2020, 1, 4);
      const newDeliveryDate = await orderUtilities.getValidDeliveryDate(orderDate);
      newDeliveryDate.toISOString().should.equals(new Date(2020, 1, 6).toISOString());
    });

    it('Delivery date is on next available working day if order date is working day and have consecutive holidays', async () => {
      const orderDate = new Date(2020, 1, 6);
      const newDeliveryDate = await orderUtilities.getValidDeliveryDate(orderDate);
      newDeliveryDate.toISOString().should.equals(new Date(2020, 1, 10).toISOString());
    });

    it('Delivery date is on next available working day after consecutive holidays if order date is holiday and next day is working', async () => {
      const orderDate = new Date(2020, 1, 11);
      const newDeliveryDate = await orderUtilities.getValidDeliveryDate(orderDate);
      newDeliveryDate.toISOString().should.equals(new Date(2020, 1, 15).toISOString());
    });

    it('Delivery date is on next available working day + 1 if order date is  holiday and have consecutive holidays', async () => {
      const orderDate = new Date(2020, 1, 13);
      const newDeliveryDate = await orderUtilities.getValidDeliveryDate(orderDate);
      newDeliveryDate.toISOString().should.equals(new Date(2020, 1, 17).toISOString());
    });
  });
});
