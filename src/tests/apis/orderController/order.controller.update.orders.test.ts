
import chai from 'chai';
import chaiHttp from 'chai-http';
// tslint:disable-next-line: import-name
import app from '../../../app/config/express';
import sinon from 'sinon';
import commonUtils from '../../utils/common.utils';
import appUtilities from '../../../app/utils/app.utilities';
import { AxiosResponse } from 'axios';

import orderValidDummy from '../../dummies/order.valid.dummy';
import { Order } from '../../../app/entities/OrderEntities/order.entity';
import { OrderStatus } from '../../../app/constants/enums/order.status.enum';
import userService from '../../../app/services/user.service';
import logger  from '../../../app/config/logger';

const should = chai.should();
const expect = chai.expect;
chai.use(chaiHttp);

describe('OrdersController update', () => {
  const createOrders = async (url: string, data: any, token?: string) => {
    return await chai
    .request(app)
    .post(url)
    .send(data)
    .set('authorization', `Bearer ${token}`);
  };

  describe('Update Orders', () => {
    let url = '/v1/orders/';
    let adminId: string;
    let customer: string;
    let tokenAdmin: string;
    let tokenCustomer: string;
    let orders: Order[];
    let args: Object = {
      url: '/v1/orders/',
      orderData: orderValidDummy,
    };
    before(async () => {
      adminId = appUtilities.generateKsuid(),
      customer = appUtilities.generateKsuid();
      tokenAdmin = commonUtils.generateTokenForRole('admin', adminId);
      tokenCustomer = commonUtils.generateTokenForRole('user', customer);
      args = { ...args, token: `${tokenCustomer}` };

      // Creating 4 new orders
      orders = await commonUtils.createMultipleOrders(createOrders, args, 5);
      url = `${url}${orders[0].id}`;
    });

    after(async () => {
      await commonUtils.clearOrderData();
    });

    it('Fail to Update status if data is "pending" -> "received" ', async () => {
      let response: any;
      const data = {
        status: OrderStatus.RECEIVED,
      };
      expect(orders[0]).to.have.property('status', OrderStatus.PENDING);
      response = await chai.request(app)
      .put(url)
      .send(data)
      .set('authorization', `Bearer ${tokenAdmin}`);
      response.should.have.status(400);
    });

    it('Fail to Update status if data is "confirmed" -> "confirmed" ', async () => {
      let response: any;
      const data = {
        status: OrderStatus.PENDING,
      };
      expect(orders[0]).to.have.property('status', OrderStatus.PENDING);
      response = await chai.request(app)
      .put(url)
      .send(data)
      .set('authorization', `Bearer ${tokenAdmin}`);
      response.should.have.status(400);
    });

    it('Update status if data is "confirmed" -> "shipped" ', async () => {
      let response: any;
      const data = {
        status: OrderStatus.SHIPPED,
      };
      let firstOrder = orders[0];
      expect(firstOrder).to.have.property('status', OrderStatus.PENDING);
      response = await chai.request(app)
      .put(url)
      .send(data)
      .set('authorization', `Bearer ${tokenAdmin}`);
      response.should.have.status(200);
      firstOrder = response.body;
      orders[0].status = firstOrder.status;
      expect(firstOrder).to.have.property('status', OrderStatus.SHIPPED);
    });

    it('Fail to Update status if data is "shipped" -> "confirmed" ', async () => {
      let response: any;
      const data = {
        status: OrderStatus.PENDING,
      };
      expect(orders[0]).to.have.property('status', OrderStatus.SHIPPED);
      response = await chai.request(app)
      .put(url)
      .send(data)
      .set('authorization', `Bearer ${tokenAdmin}`);
      response.should.have.status(400);
    });

    it('Fail to Update status if data is "shipped" -> "shipped" ', async () => {
      let response: any;
      const data = {
        status: OrderStatus.SHIPPED,
      };
      expect(orders[0]).to.have.property('status', OrderStatus.SHIPPED);
      response = await chai.request(app)
      .put(url)
      .send(data)
      .set('authorization', `Bearer ${tokenAdmin}`);
      response.should.have.status(400);
    });

    it('Fail to Update status if data is "shipped" -> "received" ', async () => {
      let response: any;
      const data = {
        status: OrderStatus.RECEIVED,
      };
      expect(orders[0]).to.have.property('status', OrderStatus.SHIPPED);
      response = await chai.request(app)
      .put(url)
      .send(data)
      .set('authorization', `Bearer ${tokenAdmin}`);
      response.should.have.status(400);
    });

    it('Update status if data is "shipped" -> "delivered" ', async () => {
      let response: any;
      const data = {
        status: OrderStatus.DELIVERED,
      };

      let firstOrder = orders[0];
      expect(firstOrder).to.have.property('status', OrderStatus.SHIPPED);
      response = await chai.request(app)
      .put(url)
      .send(data)
      .set('authorization', `Bearer ${tokenAdmin}`);
      response.should.have.status(200);
      firstOrder = response.body;
      orders[0] = firstOrder;
      expect(orders[0]).to.have.property('status', OrderStatus.DELIVERED);
    });

    it('Fail to Update status if data is "delivered" -> "shipped" ', async () => {
      let response: any;
      const data = {
        status: OrderStatus.SHIPPED,
      };
      expect(orders[0]).to.have.property('status', OrderStatus.DELIVERED);
      response = await chai.request(app)
      .put(url)
      .send(data)
      .set('authorization', `Bearer ${tokenAdmin}`);
      response.should.have.status(400);
    });

    it('Fail to Update status if data is "delivered" -> "delivered" ', async () => {
      let response: any;
      const data = {
        status: OrderStatus.DELIVERED,
      };
      expect(orders[0]).to.have.property('status', OrderStatus.DELIVERED);
      response = await chai.request(app)
      .put(url)
      .send(data)
      .set('authorization', `Bearer ${tokenAdmin}`);
      response.should.have.status(400);
    });

    it('Fail to Update status if data is "delivered" -> "confirmed" ', async () => {
      let response: any;
      const data = {
        status: OrderStatus.PENDING,
      };
      expect(orders[0]).to.have.property('status', OrderStatus.DELIVERED);
      response = await chai.request(app)
      .put(url)
      .send(data)
      .set('authorization', `Bearer ${tokenAdmin}`);
      response.should.have.status(400);
    });

    it('Update status if data is "delivered" -> "received" ', async () => {
      let response: any;
      const data = {
        status: OrderStatus.RECEIVED,
        otp: '123456',
      };
      let firstOrder = orders[0];
      expect(orders[0]).to.have.property('status', OrderStatus.DELIVERED);
      response = await chai.request(app)
      .put(url)
      .send(data)
      .set('authorization', `Bearer ${tokenAdmin}`);
      response.should.have.status(200);
      firstOrder = response.body;
      orders[0] = firstOrder;
      expect(orders[0]).to.have.property('status', OrderStatus.RECEIVED);
    });

    it('Fail to Update status if data is "recieved" -> "delivered" ', async () => {
      let response: any;
      const data = {
        status: OrderStatus.DELIVERED,
      };
      expect(orders[0]).to.have.property('status', OrderStatus.RECEIVED);
      response = await chai.request(app)
      .put(url)
      .send(data)
      .set('authorization', `Bearer ${tokenAdmin}`);
      response.should.have.status(400);
    });
  });
});
