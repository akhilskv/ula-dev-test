
import chai from 'chai';
import chaiHttp from 'chai-http';
// tslint:disable-next-line: import-name
import app from '../../../app/config/express';
import sinon from 'sinon';
import { OrderModel } from '../../../app/models/order.model';
import commonUtils from '../../utils/common.utils';
import appUtilities from '../../../app/utils/app.utilities';

import orderValidDummy from '../../dummies/order.valid.dummy';
import orderItemsDoesNotExistsDummy from '../../dummies/order.items.does.not.exists.dummy';
import lambdaService from '../../../app/services/lambda.service';

const should = chai.should();
const expect = chai.expect;
chai.use(chaiHttp);

describe('OrdersController create', () => {

  const createOrders = async (url: string, data: any, token?: string) => {
    return await chai
    .request(app)
    .post(url)
    .send(data)
    .set('authorization', `Bearer ${token}`);
  };

  sinon.stub(lambdaService, 'invokeLambdaFunction').returns(Promise.resolve({}));

  describe('Create orders', () => {
    const url = '/v1/orders';
    let testOrderValid: OrderModel;
    let testOrderInvalidWithoutItems: OrderModel;
    let customer: string;
    before(() => {
      testOrderValid = orderValidDummy;
      testOrderInvalidWithoutItems = orderItemsDoesNotExistsDummy;
      customer = appUtilities.generateKsuid();
    });

    after(async () => {
      await commonUtils.clearOrderData();
    });

    it('Unauthorized Error - if token is not passed', async () => {
      const response = await createOrders(url, testOrderValid);
      response.should.have.status(401);
    });

    it('Unauthorized Error - if role is "service"', async () => {
      const token = commonUtils.generateTokenForRole('service', customer);
      const response = await createOrders(url, testOrderValid, token);
      response.should.have.status(401);
    });

    it('Order created', async () => {
      const token = commonUtils.generateTokenForRole('user', customer);
      const response = await createOrders(url, testOrderValid, token);
      response.should.have.status(201);
    });

    it('Bad request Error - if items do not exists in order', async () => {
      const token = commonUtils.generateTokenForRole('user', customer);
      const response = await createOrders(url, testOrderInvalidWithoutItems, token);
      response.should.have.status(400);
    });
  });
});
