import chai from 'chai';
import chaiHttp from 'chai-http';
// tslint:disable-next-line: import-name
import app from '../../../app/config/express';
import commonUtils from '../../utils/common.utils';
import appUtilities from '../../../app/utils/app.utilities';

import orderValidDummy from '../../dummies/order.valid.dummy';
import orderQueryWithOffsetLimit from '../../dummies/graphql/order.query.with.offset.limit';
import orderQueryWithOrderId from '../../dummies/graphql/order.query.with.order.id';
import orderQueryWithStatusOpen from '../../dummies/graphql/order.query.with.status.open';
import { Order } from '../../../app/entities/OrderEntities/order.entity';
import { OrderStatus } from '../../../app/constants/enums/order.status.enum';
import roles from '../../../app/constants/roles';
import constants from '../../../app/constants';
import orderConstants from '../../../app/constants/OrderConstants/order.constants';
import sinon from 'sinon';
import userService from '../../../app/services/user.service';

const should = chai.should();
const expect = chai.expect;
chai.use(chaiHttp);

describe('OrdersController get', () => {
  before(() => {
    sinon.stub(userService, 'sendOtpForCustomer').returns(
      Promise.resolve({
        data: {},
        status: 0,
        statusText: '',
        headers: {},
        config: {},
      }),
    );
    sinon.stub(userService, 'verifyOtpForCustomer').returns(
      Promise.resolve({
        data: {},
        status: 0,
        statusText: '',
        headers: {},
        config: {},
      }),
    );
  });
  const createOrders = async (url: string, data: any, token?: string) => {
    return await chai
      .request(app)
      .post(url)
      .send(data)
      .set('authorization', `Bearer ${token}`);
  };

  const updateOrder = async (id: string, data: any, token?: string) => {
    const url = `/v1/orders/${id}`;
    return await chai
      .request(app)
      .put(url)
      .send(data)
      .set('authorization', `Bearer ${token}`);
  };

  after(async () => {
    await commonUtils.clearOrderData();
  });

  describe('Get orders', () => {
    const url = '/v1/graphql/';
    let customer: string;
    let token: string;
    let args: Object = {
      url: '/v1/orders/',
      orderData: orderValidDummy,
    };
    let orders: Order[];
    // let orderIds: string[];
    before(async () => {
      customer = appUtilities.generateKsuid();
      token = commonUtils.generateTokenForRole('user', customer);
      args = { ...args, token: `${token}` };

      // Creating 4 new orders
      orders = await commonUtils.createMultipleOrders(createOrders, args, 4);
      console.log('my created logging.');
    });

    it('Fetch orders with offset:2 and limit:4', async () => {
      const offset = 2;
      const limit = 4;
      const response = await chai
        .request(app)
        .post(url)
        .send(orderQueryWithOffsetLimit({ offset, limit } as any, roles.USER))
        .set('authorization', `Bearer ${token}`);
      response.should.have.status(200);
      response.body.data.orders.should.have.lengthOf(2);
    });

    it('Fetch adminOrders with offset:2 and limit:4', async () => {
      const tokenAdmin = commonUtils.generateTokenForRole('admin', appUtilities.generateKsuid());
      const offset = 2;
      const limit = 4;
      const response = await chai
        .request(app)
        .post(url)
        .send(orderQueryWithOffsetLimit({ offset, limit } as any, roles.ADMIN))
        .set('authorization', `Bearer ${tokenAdmin}`);
      response.should.have.status(200);
      response.body.data.adminOrders.should.have.lengthOf(2);
    });

    it('Fetch orders with an id', async () => {
      const id = orders[2].id;
      const response = await chai
        .request(app)
        .post(url)
        .send(orderQueryWithOrderId({ id } as any, roles.USER))
        .set('authorization', `Bearer ${token}`);
      response.should.have.status(200);
      const orderIds: string[] = response.body.data.orders.map((anOrder: Order) => {
        return anOrder.id;
      });
      expect(orderIds)
        .to.be.an('array')
        .that.contains(id);
    });

    it('Fetch adminOrders with an id', async () => {
      const tokenAdmin = commonUtils.generateTokenForRole('admin', appUtilities.generateKsuid());
      const id = orders[2].id;
      const response = await chai
        .request(app)
        .post(url)
        .send(orderQueryWithOrderId({ id } as any, roles.ADMIN))
        .set('authorization', `Bearer ${tokenAdmin}`);
      response.should.have.status(200);
      const orderIds: string[] = response.body.data.adminOrders.map((anOrder: Order) => {
        return anOrder.id;
      });
      expect(orderIds)
        .to.be.an('array')
        .that.contains(id);
    });

    it('Do not fetch orders with status "received"', async () => {
      const tokenAdmin = commonUtils.generateTokenForRole('admin', appUtilities.generateKsuid());
      const args = {
        id: orders[3].id,
        updateOrderData: {
          status: '',
          otp: '123456',
        },
        token: `${tokenAdmin}`,
      };
      await commonUtils.updateAnOrderToReceivedStatus(updateOrder, args);

      const response = await chai
        .request(app)
        .post(url)
        .send(
          orderQueryWithStatusOpen(
            { status: orderConstants.ORDER_PSEUDO_STATUS_OPEN } as any,
            roles.USER,
          ),
        )
        .set('authorization', `Bearer ${token}`);
      response.should.have.status(200);
      const orderStatuses = response.body.data.orders.map((anOrder: Order) => {
        return anOrder.status;
      });
      orderStatuses.should.have.lengthOf(3);
      expect(orderStatuses)
        .to.be.an('array')
        .that.does.not.contains(OrderStatus.RECEIVED);
    });

    it('Do not fetch adminOrders with status "received"', async () => {
      const tokenAdmin = commonUtils.generateTokenForRole('admin', appUtilities.generateKsuid());
      const response = await chai
        .request(app)
        .post(url)
        .send(
          orderQueryWithStatusOpen(
            { status: orderConstants.ORDER_PSEUDO_STATUS_OPEN } as any,
            roles.ADMIN,
          ),
        )
        .set('authorization', `Bearer ${tokenAdmin}`);
      response.should.have.status(200);
      const orderStatuses = response.body.data.adminOrders.map((anOrder: Order) => {
        return anOrder.status;
      });
      orderStatuses.should.have.lengthOf(3);
      expect(orderStatuses)
        .to.be.an('array')
        .that.does.not.contains(OrderStatus.RECEIVED);
    });
  });
});
