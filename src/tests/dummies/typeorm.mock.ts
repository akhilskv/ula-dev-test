import orderEntityMock from './order.entity.mock';

const orderRepository = {
  createOrder: async () => {
    return orderEntityMock;
  },
  getOrders: async () => {
    return [orderEntityMock];
  },
};

export const getCustomRepository = (repositoryType: any) => {
  if (repositoryType.name === 'Order' || repositoryType.name === 'OrderRepository') {
    return orderRepository;
  }
  return null;
};
export const getRepository = (repositoryType: any) => {
  if (repositoryType.name === 'Order' || repositoryType.name === 'OrderRepository') {
    return orderRepository;
  }
  return null;
};
export default {
  getCustomRepository,
  getRepository,
};
