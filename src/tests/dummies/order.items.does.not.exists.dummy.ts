const ordersInvalidWithoutItemsMockObject: any = {
  currencyCurrency: 'INR',
  address: {
    building: 'test building',
    street: 'test street',
    location: 'test location',
    city: 'test city',
    state: 'test state',
    zip: '1234567',
    country: 'test country',
  },
};

export default ordersInvalidWithoutItemsMockObject;
