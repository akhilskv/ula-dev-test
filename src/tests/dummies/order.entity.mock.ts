import { Order } from '../../app/entities/OrderEntities/order.entity';

const orderEntityMock: any = {
  id: 'OD123456789896776',
  currencyCode: 'INR',
  cartId: 'sadasdas213123',
  items: [{
    quantity: 2,
    price: 500,
    sku: 'dasdaeWWe233dcff',
    title: 'Ball',
    shortDescription: 'this is a ball.',
    thumbnailImageUrl: 'https://ula-dev-image-service.s3.ap-southeast-1.amazonaws.com/images/c4370068-cad7-44d2-8707-3389d3f5d1c0.jpg',
  },
    {
      quantity: 1,
      price: 250,
      sku: 'aaaBBdddoioo11223',
      title: 'Bat',
      shortDescription: 'this is a bat',
      thumbnailImageUrl: 'https://ula-dev-image-service.s3.ap-southeast-1.amazonaws.com/images/c4370068-cad7-44d2-8707-3389d3f5d1c0.jpg',
    },
  ],
  address: {
    building: 'builfing main',
    street: 'main street',
    location: 'main location',
    city: 'main city',
    state: 'main state',
    zip: '1234567',
    country: 'main country',
    storeName: 'testStore',
    phoneNumber: '+9100099000000',
    firstName: `test user`,
    lastName: 'test user last name',
  },
};

export default orderEntityMock;
