import orderGraphqlModel from '../../../app/models/order.graphql.model';
import roles from '../../../app/constants/roles';
import { query } from 'winston';

const createQueryWithId = (fields: orderGraphqlModel, userType: string) => {
  const queryKey = userType === roles.USER ? 'orders' : 'adminOrders';

  return {
    query: `{ \
                          ${queryKey}(id:"${fields.id}") { \
                          id \
                          customer \
                          status \
                          currencyCode \
                          shippingAmount \
                          totalAmount \
                          createdAt \
                          items { \
                          id \
                          sku \
                          name \
                          quantity \
                          price \
                          totalPrice \
                          thumbnailImageUrl \
                        }\
                      }\
                      }`,
  };

};
export default createQueryWithId;
