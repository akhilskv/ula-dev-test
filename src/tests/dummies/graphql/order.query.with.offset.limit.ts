import orderGraphqlModel from '../../../app/models/order.graphql.model';
import roles from '../../../app/constants/roles';

const createQueryWithOffsetLimit = (fields: orderGraphqlModel, userType: string) => {
  const queryKey = userType === roles.USER ? 'orders' : 'adminOrders';
  return {
    query: `{ \
                          ${queryKey}(offset: ${fields.offset}, limit: ${fields.limit}) { \
                          id \
                          customer \
                          status \
                          currencyCode \
                          shippingAmount \
                          totalAmount \
                          createdAt \
                          items { \
                          id \
                          sku \
                          name \
                          quantity \
                          price \
                          totalPrice \
                          thumbnailImageUrl \
                        }\
                      }\
                      }`,
  };

};
export default createQueryWithOffsetLimit;
