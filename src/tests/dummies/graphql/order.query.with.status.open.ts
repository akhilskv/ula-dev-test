import orderGraphqlModel from '../../../app/models/order.graphql.model';
import roles from '../../../app/constants/roles';
const getOrderQueryWithOpen = (fields: orderGraphqlModel, userType: string) => {
  const queryKey = userType === roles.USER ? 'orders' : 'adminOrders';
  return {
    query: `{ \
                          ${queryKey}(status: "${fields.status}") { \
                          id \
                          customer \
                          status \
                          currencyCode \
                          shippingAmount \
                          totalAmount \
                          createdAt \
                          items { \
                          id \
                          sku \
                          name \
                          quantity \
                          price \
                          totalPrice \
                          thumbnailImageUrl \
                        }\
                      }\
                      }`,
  };

};
export default getOrderQueryWithOpen;
