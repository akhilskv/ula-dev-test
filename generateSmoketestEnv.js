const fs = require('fs');
fs.readFile('.smoketest', 'utf8', (err, data) => {
  if (err){
      throw err;
  } else {
    const lines = data.split('\n');
    const jsonKeyArray = [];
    lines.forEach((line) => {
      const [key, value] = line.split('=');
      jsonKeyArray.push({
        key: key,
        value: value
      });
    });
  const jsonString = JSON.stringify(jsonKeyArray);
  fs.writeFile('smoketest.env.json', jsonString, 'utf8', (error, data) => {
    if(error){
      throw error;
    }
  }); // write it back 
}});