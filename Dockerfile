FROM node:10-alpine

ARG START_SCRIPT

ARG PORT

ENV START_COMMAND=$START_SCRIPT

RUN mkdir -p /home/node/app/node_modules

WORKDIR /home/node/app

COPY package*.json package-lock.json ./

RUN npm install

COPY . .

RUN npm run-script build-ts

EXPOSE $PORT

CMD npm run $START_COMMAND
